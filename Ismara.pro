TEMPLATE = app

QT += qml quick widgets network sql opengl

HEADERS +=

SOURCES += main.cpp

RESOURCES += qml.qrc

INCLUDEPATH +=

# OS X stuff
ICON = Ismara.icns
# comment the following line if working on Mac OS X < 10.11 "El Capitan"
QMAKE_MAC_SDK = macosx10.12

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = [ ".", "./plugin" ]
QML2_IMPORT_PATH = [./plugin,/home/user/Qt/5.4/gcc_64/qml,/home/partimo/Qt,plugin]
# Default rules for deployment.
include(deployment.pri)

DISTFILES +=

# Copy files to build directory
copydata0.commands = $(COPY_DIR) $$PWD/logo $$OUT_PWD
copydata.commands = $(COPY_DIR) $$PWD/LICENSE $$OUT_PWD
copydata2.commands = $(COPY_DIR) $$PWD/AUTHORS $$OUT_PWD
copydata3.commands = $(COPY_DIR) $$PWD/doc $$OUT_PWD
first.depends = $(first) copydata0 copydata copydata2 copydata3
export(first.depends)
export(copydata0.commands)
export(copydata.commands)
export(copydata2.commands)
export(copydata3.commands)
QMAKE_EXTRA_TARGETS += first copydata0 copydata copydata2 copydata3
