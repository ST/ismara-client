""" merge reads """

import argparse
import math
import os
import re
import scipy.stats.mstats


###################

def main():
    """ Main """
    parser = argparse.ArgumentParser(
        description='merge reads')

    parser.add_argument('-d',
                        help='directory with read files',
                        dest='read_dir',
                        default='expr')
    parser.add_argument('-e',
                        dest='expr_file',
                        help='File to save expression.',
                        default='expression.tab')
    parser.add_argument('-p',
                        help='pseudocount to add',
                        dest='pcount',
                        type=float,
                        default=-1000000.)

    args = parser.parse_args()

    read_files = sorted([x for x in os.listdir(args.read_dir)],
                        key=str.lower)
    reads = {}
    bed_files = []
    proms = set()
    for read_file in read_files:
        fin = open(os.path.join(args.read_dir, read_file))
        bed_file = read_file.rstrip('.expr')
        reads[bed_file] = {}
        bed_files.append(bed_file)
        for line in fin:
                entry = line.rstrip()
                if entry == '':
                        continue
                (prom_id, val) = entry.split("\t")
                reads[bed_file][prom_id] = float(val)
                proms.add(prom_id)

    total_counts = get_total_counts(reads)
    quantiles = get_quantiles(prob=0.05, reads=reads)
    if args.pcount != -1000000:
        for x in quantiles:
            quantiles[x] = args.pcount

    fout = open(os.path.join(args.read_dir, 'counts.tab'), 'w')
    fout.write("\t".join(bed_files) + "\n")
    fout.write("\t".join([str(total_counts[bed_file])
                          for bed_file in bed_files]) + "\n")
    fout.write("\t".join([str(quantiles[bed_file])
                          for bed_file in bed_files]) + "\n")

    # write counts table
    for cluster_id in proms:
        counts = []
        for bed_file in bed_files:
            if cluster_id not in reads[bed_file]:
                counts.append(0.0)
            else:
                counts.append(reads[bed_file][cluster_id])

        fout.write("%s\t%s\n" % (cluster_id,
                                 "\t".join([str(x) for x in counts])))
    fout.close()

    # add quantiles for clusters which are missing in some samples
    # while present in others
    for cluster_id in proms:
        for bed_file in bed_files:
            if cluster_id not in reads[bed_file]:
                reads[bed_file][cluster_id] = quantiles[bed_file]
            else:
                reads[bed_file][cluster_id] += quantiles[bed_file]

    table = {}
    total_counts = get_total_counts(reads)
    for bed_file in bed_files:
        for cluster_id in reads[bed_file]:
            expr = 1000000 * (reads[bed_file][cluster_id]
                              / float(total_counts[bed_file]))
            if cluster_id not in table:
                table[cluster_id] = []
            table[cluster_id].append(math.log(expr, 2))

    fout = open(args.expr_file, 'w')
    fout.write("\t".join(bed_files) + "\n")

    for cluster_id in table:
        fout.write("%s\t%s\n" % (cluster_id,
                                 "\t".join([str(x) for x in table[cluster_id]])
                                 ))
    fout.close()

    # write sample indices
    fout = open(os.path.join(args.read_dir, 'sample_indices'), 'w')
    fout.write("\n".join([re.sub("\.(bed|bam)\.?g?z?$", '', x, flags=2)
                          for x in bed_files]))
    fout.write("\n")
    fout.close()

###################


def get_quantiles(prob, reads):
    """ Get quantile for prob in avery sample in reads """
    quantiles = {}
    for sample in reads:
        quantiles[sample] = scipy.stats.mstats.mquantiles(
            [reads[sample][x] for x in reads[sample]],
            prob=[prob], alphap=0., betap=1.)[0]

    return quantiles


def get_total_counts(reads):
    """ Returns dict of total counts for samples"""
    total_count = {}

    for sample in reads:
        total_count[sample] = sum([reads[sample][x] for x in reads[sample]])

    return total_count


###################

if __name__ == '__main__':
    main()
