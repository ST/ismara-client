""" Process chipseq data in BAM and BED format """

import argparse
import bx.intervals
import bx.intervals.cluster
import gzip
import logging
logging.basicConfig(format="%(asctime)s %(filename)s:%(funcName)s: %(message)s")
log = logging.getLogger()
import os
import pysam
import re
import sys

read_shift = 100

###################


def main():
    """ Main """
    parser = argparse.ArgumentParser(
        description='Process chipseq data in BAM and BED format')

    parser.add_argument('-p',
                        help='Promoter annotation file',
                        dest='prom_file',
                        required=True)

    parser.add_argument('-i',
                        dest='input_file',
                        help='input file',
                        required=True)
    parser.add_argument('-e', '--expr-dir',
                        help='dir to keep expr files',
                        dest='expr_dir',
                        default='expr')
    parser.add_argument('-d',
                        help="show debug messages",
                        dest="debug",
                        action='store_true',
                        default=False)
    parser.add_argument('-w',
                        help="Window size. Left and right distance to cover, space separetd, or one number = window width. In last case window is symetric around promoter",
                        dest="window",
                        nargs="+",
                        default=[2000])
    args = parser.parse_args()

    if args.debug:
        log.setLevel(logging.DEBUG)

    log.warning("Started!")
    if len(args.window) == 1:
        args.window = [int(args.window[0]) / 2, int(args.window[0]) / 2]
    else:
        args.window = [int(x) for x in args.window]
    proms = Regions(prom_file=args.prom_file,
                    window=args.window)
    log.warning("Promoters initialized!.")
    readfile = openReadFile(args.input_file)

    counter = 0
    log.warning("starting read processing")
    for chrom in proms.regionTree:
        regions = proms.regionClusters[chrom].getregions()
        for (start, end, region_id) in regions:
            region = (chrom, start, end)
            #logging.warning("region: %s", str(region))
            try:
                reads = processReads(readRegion(readfile, region))
            except ValueError:
                    continue
            assign_reads(reads, proms, region)
            counter += 1
            if (counter % 1000) == 0:
                log.warning("%d regions read", counter)

    log.warning("all regions processed")

    # write output
    if not os.path.exists(args.expr_dir):
        try:
            os.makedirs(args.expr_dir)
        except:
            pass
    fout = open("%s/%s.expr" % (args.expr_dir,
                                os.path.basename(args.input_file)), 'w')
    for prom in proms.counts:
        if proms.counts[prom] > 0.:
            fout.write("%s\t%f\n" % (prom, proms.counts[prom]))
    fout.close()
    log.warning("All done!")


###################

class Regions:
    regionTree = {}
    regionClusters = {}
    counts = {}

    def __init__(self, prom_file=None, window=[1000, 1000]):
        """ Init """
        if prom_file:
            self.read_promoters(prom_file, window)

    def read_promoters(self, prom_file, window):
        """ Parse prom file and create promoter tree """
        fin = multi_open(prom_file)
        print window
        for line in fin:
            if line.strip().startswith('#'):
                continue
            data = line.rstrip().split("\t")
            prom_id = data[5]
            chrom = data[0]
            start = int(data[2])
            end = int(data[3])
            mid = start + int((end - start + 1) / 2)
            region_start = mid - window[0]
            region_end = mid + window[1] - 1

            if prom_id in self.counts:
                raise(BaseException("Duplicated entries for cluster %s\n"
                                     % prom_id))
            self.counts[prom_id] = 0.

            if chrom not in self.regionTree:
                self.regionTree[chrom] = bx.intervals.IntervalTree()
                self.regionClusters[chrom] = bx.intervals.cluster.ClusterTree(1, 0)

            # add region to the tree
            self.regionClusters[chrom].insert(region_start, region_end, 0)
            self.regionTree[chrom].add(region_start - 1,
                                       region_end + 1,
                                       (prom_id, 1))


###################

def multi_open(file_name):
    """ Open file differently depending on file extension """

    if re.search("\.gz$", file_name):
        fin = gzip.open(file_name)
    elif file_name == 'stdin':
        fin = sys.stdin
    else:
        fin = open(file_name)

    return fin


def readRegion(readfile, region):
    """ read region from SAM file and return reads from the region """
    return [x for x in readfile.fetch(region[0], region[1] - read_shift, region[2] + read_shift)]


def openReadFile(file_name):
    """ depending on extension create corresponding file object """
    if file_name.lower().endswith('.bam'):
        return pysam.Samfile(file_name, 'rb')
    elif file_name.lower().endswith('.sam') or file_name.lower().endswith('.sam.gz') :
        return pysam.Samfile(file_name)
    elif file_name.lower().endswith('.bed') or file_name.lower().endswith('.bed.gz'):
        return pysam.Tabixfile(file_name)
    else:
        raise(BaseException("Could not open file %s. File should have .bam, .sam or .bed extension" % file_name))


def processReads(reads):
    """ Translate reads to list of tuples, collapse same reads """
    new_reads = {}
    paired_reads = {}

    # skip if there are no reads
    if len(reads) == 0:
        return reads

    # process reads depending on type BAM or BED
    if isinstance(reads[0], pysam.AlignedRead) or isinstance(reads[0], pysam.calignmentfile.AlignedSegment): 
        for read in reads:
            if read.is_paired:
                if read.is_proper_pair:
                    if read.qname not in paired_reads:
                        strand = '+'
                        if read.is_read1 and read.is_reverse:
                            strand = '-'
                        elif read.is_read2 and not read.is_reverse:
                            strand = '-'
                        paired_reads[read.qname] = [strand, 1]
                    else:
                        paired_reads[read.qname][1] = 2
                    cigar = read.cigar
                    pos = read.pos + 1
                    #shift coordinates
                    if paired_reads[read.qname][0] == "+":
                        pos += read_shift
                    else:
                        pos -= read_shift
                    blocks = []
                    for (op, length) in cigar:
                        if op in [0, 7, 8]:
                            if blocks and blocks[-1][1] == pos - 1:
                                blocks[-1] = (blocks[-1][0], pos + length - 1)
                            else:
                                blocks.append((pos, pos + length - 1))
                            pos = pos + length
                        elif op in [2, 3, 6]:
                            pos = pos + length
                    paired_reads[read.qname].extend(blocks)
                else:
                    continue
            else:
                strand = '+'
                if read.is_reverse:
                    strand = '-'
                # check read for gaps
                #blocks = get_blocks(read)
                cigar = read.cigar
                pos = read.pos + 1
                #shift coordinates
                if strand == "+":
                    pos += read_shift
                else:
                    pos -= read_shift

                blocks = []
                for (op, length) in cigar:
                    if op in [0, 7, 8]:
                        if blocks and blocks[-1][1] == pos - 1:
                            blocks[-1] = (blocks[-1][0], pos + length - 1)
                        else:
                            blocks.append((pos, pos + length - 1))
                        pos = pos + length
                    elif op in [2, 3, 6]:
                        pos = pos + length
                blocks.append(strand)
                #log.warning("cigar: %s, rstart: %d, qlen: %d, alen: %d,blocks: %s" %
                #            (cigar, read.pos + 1, read.qlen, read.alen, str(blocks)))
                new_read = (tuple(blocks))
                if new_read in new_reads:
                    new_reads[new_read] += 1.
                else:
                    new_reads[new_read] = 1.
    elif isinstance(reads[0], str):
        for read in reads:
            data = read.split("\t")
            if len(data) >= 6:
                strands = [data[5]]
            elif len(data) == 3:
                # support for non-stranded RNA-Seq
                strands = ['+', '-']
            else:
                raise(BaseException('Could not guess strand for read %s'
                                    % read))
            for strand in strands:
                new_read = ((int(data[1]) + 1, int(data[2])), strand)
                #add_read(new_reads, new_read)
                if new_read in new_reads:
                    new_reads[new_read] += 1.
                else:
                    new_reads[new_read] = 1.
    else:
        raise(BaseException("Could not determine read type!"))

    # if there are paired reads include ones with both mates in the region
    if paired_reads:
        for name in paired_reads:
            if paired_reads[name][1] != 2:
                continue
            new_read = tuple(paired_reads[name][2:] + paired_reads[name][:1])
            if new_read in new_reads:
                new_reads[new_read] += 1.
            else:
                new_reads[new_read] = 1.

    return new_reads


def assign_reads(reads, proms, region):
    (chrom, rstart, rend) = region
    tree = proms.regionTree[chrom]
    for read in reads:
        #if len(read) > 2:
            # logging.debug("chrom: %s read: %s %f",
            #               chrom,
            #               str(read),
            #               reads[read])
        troverlap = []
        for (start, end) in read[:-1]:
            soverlap = set(tree.find(start, start))
            eoverlap = set(tree.find(end, end))
            data = set.intersection(soverlap, eoverlap)
            #if len(read) > 2:
                # logging.debug("subread start - end: %d - %d" % (start, end))
                # logging.debug("soverlap: %s", str(soverlap))
                # logging.debug("eoverlap: %s", str(eoverlap))
                # logging.debug("data: %s", str(data))
            # exit if empty set
            if not data:
                break
            troverlap.append(set([(x[0], x[1]) for x in data]))
        # skip read if one of parts not mapped to the same exon
        #if len(read) > 2:
            #logging.debug("troverlap: %s", str(troverlap))
        if len(troverlap) != len(read[:-1]):
            continue

        x = set.intersection(*troverlap)
        tr_num = len(x)
        #if len(read) > 2:
            #logging.debug("tr_num: %d", tr_num)
        for (prom, i) in x:
                proms.counts[prom] += reads[read] / tr_num

###################

if __name__ == '__main__':
    main()
