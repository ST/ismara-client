#!/bin/bash

# Genome (mm9, hg19, hg18)
# @Internal: For now no check.
# @Internal: Maybe we should check:
# @Internal: whether the var is empty
# @Internal: whether it is equal to mm9, hg19 or hg18
# @Internal: as the script will be run within ISMARA, it may be an overkill
# @Internal: Check the scenarios (depending on what we decide for the processing of data)
export genome=$1

# Possibility to choose between sitecount matrix with/without microRNAs.
# @Internal: Corresponding sitecount matrices are:
# @Internal: siteCounts/SwissReg_TargetScan/sitecount_matrix_allProms
# @Internal: ==> Include miRNA
# @Internal: siteCounts/SwissReg/sitecount_matrix_allProms
# @Internal: ==> Do not include miRNA
micro=$2

# JOBID
JOBID=$3
# get paths
PATHS=$4

# Retrieve the script name from the script's base name
programName=$(basename "$0" | cut -d. -f1)
export ROOT=$(dirname $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ))

source $ROOT/scripts/common_utils.sh
source $ROOT/scripts/env.sh $programName $PATHS

######################################
# max number of cpu cores to use     #
# default is physical core count - 1 #
######################################

# process the files in cpu-1 processes
export cpus=$([[ $(uname) = 'Darwin' ]] && 
                       sysctl -n hw.physicalcpu_max ||
                       lscpu -p | egrep -v '^#' | sort -u -t, -k 2,4 | wc -l)

phymem=$([[ $(uname) = 'Darwin' ]] && 
                       sysctl -n hw.memsize ||
                       free -g | awk '/^Mem:/{print $2}')

# do we have memory in bytes?
if [[ $phymem -gt 1000 ]]; then
    export phymem=$((phymem / 10**9))
else
    export phymem
fi

# replace to override
cpus_to_use=$((cpus))
#cpus_to_use=8

#export phymem=32

if [[ $cpus_to_use*2 -lt $phymem-2 ]]; then
    export mem_limit='2G'
elif [[ $cpus_to_use*1.5 -lt $phymem-2 ]]; then
    export mem_limit='1500M'
else
    export mem_limit='500M'
fi

array=( $@ )
len=${#array[@]}

check-input-files array || json_out "error" 1 "Input file error" true 1

# step X/Y
steps_outside_file_loop=3

# progress bar, or cost of operation "progress 4" all summed up outside loops
items_outside_file_loop=2

# how many steps (progress bar) there are for one file summed up
items_for_files=2

# number of files
number_of_files=$((len - 4))

# progress length of recurring file operations
items_inside_file_loop_sum=$((number_of_files * items_for_files))

# how many steps per file in a loop # step X/Y of file 1/z
steps_for_files_in_loop=2

# steps for files inside loop summed
# steps_for_files_sum=$((steps_for_files_in_loop * number_of_files))

# take cores into account
steps_for_files_sum=$(((number_of_files / cpus_to_use) + (number_of_files % cpus_to_use > 0)))+1

# can't report steps from individual files from child process so use 1
#steps_for_files_sum=1

# how many progress steps in total
steps_total=$((steps_outside_file_loop + steps_for_files_sum))
#we hardcode the main steps for now
#steps_total=4

# total of reported steps
progress_steps_main_total=$((steps_for_files_sum + progress_functions_exluding_per_file - progress_steps_for_files_in_loop))

items_total=$((items_outside_file_loop + items_inside_file_loop_sum))

filestep=1

json_out "progress_total" 1 $items_total false

json_out "info" 1 "root is set to $ROOT" true

json_out "info" 1 "Using $cpus_to_use cores, $mem_limit / core from total memory of $phymem" true

if [ $len -lt 6 ]; then
    json_out "error" 1 "Error: At least 2 files are required!" true 1
fi

json_out "info" 1 "Start workflow" true
# @Internal: use severity 2 for progress that shows up in progress bar
# @Internal: first step, create directories, cost 1
json_out "info" 2 "STEP $filestep/$steps_total: Creating job directories" false

json_out "progress" 2 1 false

mkdir -p $OUTPUT
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't create directory "$OUTPUT true 1
[[ ! -w "$OUTPUT" ]] && json_out "error" 1 "Can't write to directory "$OUTPUT true 1

mkdir -p $TMP
[[ ! -d "$TMP" ]] && json_out "error" 1 "Can't create directory "$TMP true 1
[[ ! -w "$TMP" ]] && json_out "error" 1 "Can't write to directory "$TMP true 1

mkdir -p $TMP/processing
[[ ! -d "$TMP/processing" ]] && json_out "error" 1 "Can't create directory "$TMP/processing true 1
[[ ! -w "$TMP/processing" ]] && json_out "error" 1 "Can't write to directory "$TMP/processing true 1

mkdir -p $TMP/expression
[[ ! -d "$TMP/expression" ]] && json_out "error" 1 "Can't create directory "$TMP/expression true 1
[[ ! -w "$TMP/expression" ]] && json_out "error" 1 "Can't write to directory "$TMP/expression true 1

cd $IPATH

# get files into array, (arrays first 4 items are not files)
files=("${array[@]:4}")
num_of_files=${#files[@]}

json_out "info" 2 "STEP $((filestep+1))/$steps_total: Formatting and creating indexes for $num_of_files files. (This may take a while)" true
json_out "progress" 2 2 false
# take source files and feed them to processing script in parallel
printf "%s\n" "${files[@]}" | xargs -n1 -P$cpus_to_use -I '{}' $ROOT/scripts/parallel_rnaseq.sh {}

json_out "info" 1 "All files indexed and merged." true

# @Internal: after indexing and sorting file operations are more or less done, update progress bar for all of them at once
# @Internal: TODO try to get progress from child process for more accurate progress bar
json_out "progress" 2 $steps_for_files_sum false

# @Internal: step 3 cost 2

mergeReads_Cmd="$IPATH/merge_reads.py -d $TMP/expression -e $TMP/expression/expression.tab" 

filestep=$((filestep + steps_for_files_sum))
json_out "info" 2 "STEP $filestep/$steps_total: merging reads" false


filestep=$((filestep + 1))

python $mergeReads_Cmd || json_out "error" 1 "Script $0 Line $LINENO: Problem with: python $mergeReads_Cmd Error: $( { python $mergeReads_Cmd; } )" true $?

json_out "progress" 2 2 false

# @Internal: step 4 cost 2
json_out "info" 2 "STEP $filestep/$steps_total: Preparing output" false
json_out "info" 1 "output dir: "$OUTPUT$JOBID true

mkdir $OUTPUT$JOBID || json_out "error" 1 "Error creating dir: "$OUTPUT$JOBID true 1
[[ ! -d "$OUTPUT$JOBID" ]] && json_out "error" 1 "Can't create directory $OUTPUT$JOBID" true 1
[[ ! -w "$OUTPUT$JOBID" ]] && json_out "error" 1 "Can't write to directory $OUTPUT$JOBID" true 1

mv $TMP/expression/expression.tab $OUTPUT/$JOBID || json_out "error" 1 "Cannot move $TMP/expression.tab to $OUTPUT/$JOBDIR" true 1

json_out "progress" 2 2 false

filestep=$((filestep + 1))
json_out "info" 2 "STEP $filestep/$steps_total: preparing upload" false

json_out "info" 1 "End workflow" true
json_out "info" 1 "Final result is available in $OUTPUT$JOBID/expression.tab and will be uploaded to ISMARA" true

json_out "info" 1 "Cleaning up. Deleting temp files from $TMP" true

rm -rf $TMP
json_out "success" 1 1 false
