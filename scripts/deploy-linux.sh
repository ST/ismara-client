#!/bin/bash

# create the ismara_installer?
echo " "
read -p "Create the ismara_installer ? [y/n]: " createInstaller;
if [[ $createInstaller == "n" ]]
then
echo "The ismara_installer wil not be created"
elif [[ $createInstaller == "y" ]]
then
read -p "Absolute path to the Qt installer framework:
(leave empty for default: /home/st-user/Qt/QtIFW2.0.0)" answer0;
	if [[ -z $answer0 ]]
	then
	export QT_IFW_PATH=/home/st-user/Qt/QtIFW2.0.0
	else
	export QT_IFW_PATH=$answer0
	fi
else
echo "Please answer y or n !"; exit
fi
echo " "

# get version and paths
echo " "
read -p "Version tag: v" answer;
if [[ -z "$answer" ]]
then
echo "Version tag is required, sorry!"; exit
else
VERSION=$answer
fi
echo " "

read -p "Absolute path to the QT installation:
(leave empty for default: /home/st-user/Qt)" answer1;
if [[ -z "$answer1" ]]
then
export QT_INSTALL_PATH=/home/st-user/Qt
else
export QT_INSTALL_PATH=$answer1
fi
echo "QT_INSTALL_PATH: $QT_INSTALL_PATH"
echo " "

read -p "Absolute path to the Ismara-client-release (build path):
(leave empty for default: /home/st-user/ismara-client-release)" answer2;
if [[ -z "$answer2" ]]
then
export ISMARA_BUILD_PATH=/home/st-user/ismara-client-release
else
export ISMARA_BUILD_PATH=$answer2
fi
echo "export ISMARA_BUILD_PATH: $ISMARA_BUILD_PATH"
echo " "

read -p "Absolute path to the deployed Ismara client:
(leave empty for default: /home/st-user/ismara-client-v$VERSION)" answer3;
if [[ -z "$answer3" ]]
then
export ISMARA_INSTALL_PATH=/home/st-user/ismara-client-v$VERSION
else
export ISMARA_INSTALL_PATH="$answer3-v$VERSION"
fi
echo "ISMARA_INSTALL_PATH: $ISMARA_INSTALL_PATH"
echo " "

read -p "Absolute path to the ISMARA-client-ismara_installer_package:
(leave empty for default: /home/st-user/ismara-client/ismara_installer_package)" answer4;
if [[ -z "$answer4" ]]
then
export ISMARA_INSTALLER_PATH=/home/st-user/ismara-client/ismara_installer_package
else
export ISMARA_INSTALLER_PATH="$answer4"
fi
echo "ISMARA_INSTALLER_PATH: $ISMARA_INSTALLER_PATH"
echo " "

# TODO use qmake instead to create binary in source tree
#export ISMARA_BUILD_PATH=/home/st-user/ismara-client-release
#export ISMARA_INSTALL_PATH=/home/st-user/ismara-client-f264fa10

[[ ! -d "$QT_INSTALL_PATH" ]] && exit 1
[[ ! -w "$QT_INSTALL_PATH" ]] && exit 1
#cd $QT_INSTALL_PATH || exit 1


# TODO we assume that the input data is already availble in 
# $ISMARA_INSTALL_PATH/data
# if not, you can download it from 
# https://rxtx.isb-sib.ch/public.php?service=files&t=e4ae2e9a50749575c0e4e693b70b2f55 

mkdir -p $ISMARA_INSTALL_PATH/libs
mkdir -p $ISMARA_INSTALL_PATH/platforms
mkdir -p $ISMARA_INSTALL_PATH/qml
mkdir -p $ISMARA_INSTALL_PATH/bin
mkdir -p $ISMARA_INSTALL_PATH/logs
mkdir -p $ISMARA_INSTALL_PATH/tmp
mkdir -p $ISMARA_INSTALL_PATH/sqldrivers

cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libicui18n.so.53  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libQt5Core.so.5  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libQt5Gui.so.5  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libQt5Qml.so.5  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libQt5Widgets.so.5  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libicudata.so.53  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libicuuc.so.53  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libQt5DBus.so.5  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libQt5Network.so.5  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libQt5Quick.so.5  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/lib/libQt5Sql.so.5  $ISMARA_INSTALL_PATH/libs
cp $QT_INSTALL_PATH/5.4/gcc_64/plugins/sqldrivers/libqsqlite.so  $ISMARA_INSTALL_PATH/sqldrivers
cp /usr/lib/lapack/liblapack.so* $ISMARA_INSTALL_PATH/libs
cp /usr/lib/libblas/libblas.so* $ISMARA_INSTALL_PATH/libs
cp /usr/lib/x86_64-linux-gnu/libgfortran.so.3* $ISMARA_INSTALL_PATH/libs
cp /usr/lib/libblas/libblas.so* $ISMARA_INSTALL_PATH/libs
                       
cp $QT_INSTALL_PATH/5.4/gcc_64/plugins/platforms/libqxcb.so $ISMARA_INSTALL_PATH/platforms

cp -r $QT_INSTALL_PATH/5.4/gcc_64/qml $ISMARA_INSTALL_PATH

cp -r $ISMARA_BUILD_PATH/data $ISMARA_INSTALL_PATH
cp -r $ISMARA_BUILD_PATH/scripts $ISMARA_INSTALL_PATH
cp -r $ISMARA_BUILD_PATH/doc $ISMARA_INSTALL_PATH
cp -r $ISMARA_BUILD_PATH/AUTHORS $ISMARA_INSTALL_PATH
cp -r $ISMARA_BUILD_PATH/LICENSE $ISMARA_INSTALL_PATH
cp -r $ISMARA_BUILD_PATH/logo $ISMARA_INSTALL_PATH
rm -rf $ISMARA_INSTALL_PATH/scripts/python_libs/osx
rm -rf $ISMARA_INSTALL_PATH/scripts/R_libs/osx
rm -r $ISMARA_INSTALL_PATH/scripts/deploy-linux.sh
rm -r $ISMARA_INSTALL_PATH/scripts/deploy-osx.sh
cp -r $ISMARA_BUILD_PATH/utils $ISMARA_INSTALL_PATH
rm -rf $ISMARA_INSTALL_PATH/utils/osx
cp $ISMARA_BUILD_PATH/Ismara $ISMARA_INSTALL_PATH

#echo "export LD_LIBRARY_PATH=$ISMARA_INSTALL_PATH/libs:$ISMARA_INSTALL_PATH/platforms:${LD_LIBRARY_PATH}" > $ISMARA_INSTALL_PATH/bin/start.ismara
#echo "export QML2_IMPORT_PATH=$ISMARA_INSTALL_PATH/qml" >> $ISMARA_INSTALL_PATH/bin/start.ismara
echo '#!/bin/bash' > $ISMARA_INSTALL_PATH/bin/start.ismara 
echo 'ROOT=$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )")' >> $ISMARA_INSTALL_PATH/bin/start.ismara
echo 'export LD_LIBRARY_PATH=$ROOT/libs:$ROOT/platforms:${LD_LIBRARY_PATH}' >> $ISMARA_INSTALL_PATH/bin/start.ismara
echo 'export QML2_IMPORT_PATH=$ROOT/qml' >> $ISMARA_INSTALL_PATH/bin/start.ismara 

# the application should stay in the root directory
# and not be moved to the bin directory
# indeed, in such case, the root is $root/bin which breaks the paths
# in the shell scripts 
#echo "$ISMARA_INSTALL_PATH/Ismara" >> $ISMARA_INSTALL_PATH/bin/start.ismara
echo '$ROOT/Ismara' >> $ISMARA_INSTALL_PATH/bin/start.ismara

chmod u+x $ISMARA_INSTALL_PATH/bin/start.ismara

cd $ISMARA_INSTALL_PATH/bin
ln -s ../platforms platforms

if [[ $createInstaller == "y" ]]
then
cd $HOME
cp -r $ISMARA_INSTALLER_PATH $HOME
cd $HOME/ismara_installer_package
DATE=`date +%Y-%m-%d`
perl -pi -e "s/XXVERSION/$VERSION/g" packages/com.sib.ismara/meta/package.xml 
perl -pi -e "s/XXDATE/$DATE/g" packages/com.sib.ismara/meta/package.xml 
perl -pi -e "s/XXVERSION/$VERSION/g" config/config.xml
cat config/config.xml
mkdir -p packages/com.sib.ismara/data
cp -r $ISMARA_INSTALL_PATH/* packages/com.sib.ismara/data
echo "Now building ISMARA-client-v$VERSION-Ubuntu-15.04-installer-x64.run"
$QT_IFW_PATH/bin/binarycreator -c config/config.xml -p packages ISMARA-client-v$VERSION-Ubuntu-15.04-installer-x64.run
fi
echo " "

