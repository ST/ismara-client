#!/bin/bash

trap 'kill $(jobs -p)' EXIT

export LOG=$2

export ROOT=$(dirname $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ))

source "$ROOT"/scripts/common_utils.sh

FILEPATH=$1
FILE=$(basename $FILEPATH)

format-and-index $FILEPATH $FILE

json_out "info" 1 "End formatting and creating index file: $FILE" true
json_out "info" 1 "Running readRegion.py on $FILE" true
json_out "progress" 2 2 false

# hardcoded number of this step
json_out "info" 2 "STEP 3: Reading regions" true

export chipseq_readRegion_Cmd="$IPATH/chipseq_readRegion.py -p $INPUT/$genome/clusters_150_gene_associations.gz -i $TMP/processing/$FILE --expr-dir $TMP/expression"
python $chipseq_readRegion_Cmd || json_out "error" 1 "Error with $chipseq_readRegion_Cmd" true 1
json_out "info" 1 "End chipseq_readRegion for $FILE" true
json_out "progress" 2 3 false
