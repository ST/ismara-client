""" Functions to read specified regions from BAM/SAM/BED """

import argparse
import bx.intervals
import bx.intervals.cluster
import gzip

import logging
logging.basicConfig(format="%(asctime)s %(filename)s:%(funcName)s: %(message)s")
log = logging.getLogger()

import os
import pysam
import re
import sys


def main():
    parser = argparse.ArgumentParser(
        description='Make expression table out of reads.')

    parser.add_argument('-t',
                        help='UCSC transcript file',
                        dest='tr_files',
                        required=True,
                        nargs='+')
    parser.add_argument('-p',
                        help='Promoter annotation file',
                        dest='prom_file',
                        required=True)
    parser.add_argument('-i',
                        dest='input_file',
                        help='input file',
                        required=True)
    parser.add_argument('-e', '--expr-dir',
                        help='dir to keep expr files',
                        dest='expr_dir',
                        default='expr')
    parser.add_argument('-d',
                        help="show debug messages",
                        dest="debug",
                        action='store_true',
                        default=False)
    args = parser.parse_args()

    if args.debug:
        log.setLevel(logging.DEBUG)

    log.warning("Started!")
    proms = Promoters(prom_file=args.prom_file,
                      tr_files=args.tr_files)
    log.warning("Promoters initialized!.")
    readfile = openReadFile(args.input_file)
    use_numbers = False
    if args.input_file.lower().endswith('bam') or args.input_file.lower().endswith('sam') or args.input_file.lower().endswith('.sam.gz'):
        use_numbers = have_numbers(readfile)
    elif args.input_file.lower().endswith('.bed') or args.input_file.lower().endswith('.bed.gz'):
        use_numbers = have_numbers_bed(readfile)
    counter = 0
    log.warning("starting read processing")
    for chrom in proms.trCluster:
        for strand in proms.trCluster[chrom]:
            regions = proms.trCluster[chrom][strand].getregions()
            for (start, end, id) in regions:
                #region = (chrom, strand, start, end)
                chromm = chrom
                if use_numbers:
                    chromm = chrom.replace('chr', '')
                region = (chrom, strand, start - 2, end + 2)
                #log.debug("region: %s", str(region))
                try:
                    reads = processReads(readRegion(readfile,
                                                    (chromm,
                                                     strand,
                                                     start,
                                                     end)))
                except ValueError:
                    continue
                assign_reads(reads, proms, region)
                counter += 1
                if (counter % 1000) == 0:
                    log.warning("%d regions read", counter)

    log.warning("all regions processed")

    # write output
    if not os.path.exists(args.expr_dir):
        os.makedirs(args.expr_dir)
    fout = open("%s/%s.expr" % (args.expr_dir,
                                os.path.basename(args.input_file)), 'w')
    for prom in proms.counts:
        if proms.counts[prom] > 0.:
            fout.write("%s\t%f\n" % (prom, proms.counts[prom]))
    fout.close()
    log.warning("All done!")


def readRegion(readfile, region):
    """ read region from SAM file and return reads from the region """
    return [x for x in readfile.fetch(region[0], region[2], region[3])]


def openReadFile(file_name):
    """ depending on extension create corresponding file object """
    if file_name.lower().endswith('.bam'):
        return pysam.Samfile(file_name, 'rb')
    elif file_name.lower().endswith('.sam') or file_name.lower().endswith('.sam.gz') :
        return pysam.Samfile(file_name)
    elif file_name.lower().endswith('.bed') or file_name.lower().endswith('.bed.gz'):
        return pysam.Tabixfile(file_name)
    else:
        raise(BaseException("Could not open file %s. File should have .bam, .sam or .bed extension" % file_name))


def processReads(reads):
    """ Translate reads to list of tuples, collapse same reads """
    new_reads = {}
    paired_reads = {}

    # skip if there are no reads
    if len(reads) == 0:
        return reads

    def get_blocks(read):
        """ get coordinate blocks from read """
        cigar = read.cigar
        rstart = read.pos + 1
        rend = read.pos + read.qlen
        pos = rstart
        blocks = []
        if 3 in [x[0] for x in cigar]:
            for (op, length) in cigar:
                if op in [0, 7, 8]:
                    blocks.append((pos, pos + length - 1))
                    pos = pos + length
                elif op in [2, 3, 6]:
                    pos = pos + length
            if blocks[-1][-1] != rend:
                raise(BaseException("cigar: %s, rstart: %d, rend: %d, blocks: %s" %
                                    (cigar, rstart, rend, str(blocks))))

        else:
            blocks = [(rstart, rend)]

        return blocks

    # process reads depending on type BAM or BED
    if isinstance(reads[0], pysam.AlignedRead) or isinstance(reads[0], pysam.calignmentfile.AlignedSegment):
        for read in reads:
            if read.is_paired:
                if read.is_proper_pair:
                    if read.qname not in paired_reads:
                        strand = '+'
                        if read.is_read1 and read.is_reverse:
                            strand = '-'
                        elif read.is_read2 and not read.is_reverse:
                            strand = '-'
                        paired_reads[read.qname] = [strand, 1]
                    else:
                        paired_reads[read.qname][1] = 2
                    #blocks = get_blocks(read)
                    cigar = read.cigar
                    pos = read.pos + 1
                    blocks = []
                    for (op, length) in cigar:
                        if op in [0, 7, 8]:
                            if blocks and blocks[-1][1] == pos - 1:
                                blocks[-1] = (blocks[-1][0], pos + length - 1)
                            else:
                                blocks.append((pos, pos + length - 1))
                            pos = pos + length
                        elif op in [2, 3, 6]:
                            pos = pos + length
                    paired_reads[read.qname].extend(blocks)
                else:
                    continue
            else:
                strand = '+'
                if read.is_reverse:
                    strand = '-'
                # check read for gaps
                #blocks = get_blocks(read)
                cigar = read.cigar
                pos = read.pos + 1
                blocks = []
                for (op, length) in cigar:
                    if op in [0, 7, 8]:
                        if blocks and blocks[-1][1] == pos - 1:
                            blocks[-1] = (blocks[-1][0], pos + length - 1)
                        else:
                            blocks.append((pos, pos + length - 1))
                        pos = pos + length
                    elif op in [2, 3, 6]:
                        pos = pos + length
                blocks.append(strand)
                #log.warning("cigar: %s, rstart: %d, qlen: %d, alen: %d,blocks: %s" %
                #            (cigar, read.pos + 1, read.qlen, read.alen, str(blocks)))
                new_read = (tuple(blocks))
                if new_read in new_reads:
                    new_reads[new_read] += 1.
                else:
                    new_reads[new_read] = 1.
    elif isinstance(reads[0], str):
        for read in reads:
            data = read.split("\t")
            if len(data) >= 6:
                strands = [data[5]]
            elif len(data) == 3:
                # support for non-stranded RNA-Seq
                strands = ['+', '-']
            else:
                raise(BaseException('Could not guess strand for read %s'
                                    % read))
            for strand in strands:
                new_read = ((int(data[1]) + 1, int(data[2])), strand)
                #add_read(new_reads, new_read)
                if new_read in new_reads:
                    new_reads[new_read] += 1.
                else:
                    new_reads[new_read] = 1.
    else:
        raise(BaseException("Could not determine read type!"))

    # if there are paired reads include ones with both mates in the region
    if paired_reads:
        for name in paired_reads:
            if paired_reads[name][1] != 2:
                continue
            new_read = tuple(paired_reads[name][2:] + paired_reads[name][:1])
            if new_read in new_reads:
                new_reads[new_read] += 1.
            else:
                new_reads[new_read] = 1.

    return new_reads


class Promoters:
    tr2proms = {}
    counts = {}
    exonTree = {}
    trCluster = {}
    tr_count = 0
    transcripts = {}
    
    def __init__(self, prom_file=None, tr_files=None):
        """ Init promoters """
        if prom_file:
            self.read_promoters(prom_file)

        if tr_files:
            for tr_file in tr_files:
                if tr_file.lower().endswith(".gff") or tr_file.lower().endswith(".gtf") \
                   or tr_file.lower().endswith(".gff.gz") or tr_file.lower().endswith(".gtf.gz"):
                    self.read_gtf_transcripts(tr_file)
                else:
                    self.read_all_mrna(tr_file)

    def read_promoters(self, prom_file):
        """ Read and process cluster file """
        fin = multi_open(prom_file)

        for line in fin:
            if line.strip().startswith("#"):
                continue

            data = line.rstrip().split("\t")
            prom_id = data[5]

            transcripts = [x for x in data[9].split()
                           if not x.startswith("TSC_")]

            if prom_id in self.counts:
                raise(BaseException("Duplicated entries for cluster %s\n"
                                     % prom_id))

            self.counts[prom_id] = 0.

            for transcript in transcripts:
                if transcript not in self.tr2proms:
                    self.tr2proms[transcript] = set()
                self.tr2proms[transcript].add(prom_id)

    def read_all_mrna(self, transcript_file):
        """ Read and process all_mrna file """
        fin = multi_open(transcript_file)

        for line in fin:
            transcript = self.parse_all_mrna_line(line)

            # check if we need this transcript
            transcript_id = transcript[0]
            if transcript_id in self.tr2proms:
                self.add_transcript(transcript)

    def parse_all_mrna_line(self, line):
        """ Parse all_mrna line """

        data = line.rstrip().split('\t')
        transcript_id = data[10]
        chrom = data[14]
        strand = data[9]

        (tr_start, tr_end) = sorted([int(data[16]) + 1, int(data[17])])
        exon_starts = [int(x) + 1 for x in data[21].rstrip(',').split(',')]
        block_sizes = [int(x) for x in data[19].rstrip(',').split(',')]
        exon_ends = [x[0] + x[1] - 1 for x in zip(exon_starts, block_sizes)]

        # check if last exon end equal to tr_end
        if exon_ends[-1] != tr_end or exon_starts[0] != tr_start:
            raise(BaseException(
                "Transcript end and last exon end do not coincide for:\n%s\n"
                % (line)))

        exons = zip(exon_starts, exon_ends)
        transcript = (transcript_id, chrom, strand, tr_start, tr_end, exons)

        return transcript


    def read_gtf_transcripts(self, transcript_file):
        """ Read and process gtf/gff file (for gtf files from gencode project) """
        fin = multi_open(transcript_file)

        for line in fin:
            if line.strip().startswith('#'):
                continue
            data = line.rstrip().split('\t')
            if data[2] == 'transcript':
                chrom = data[0]
                strand = data[6]
                (tr_start, tr_end) = sorted([int(data[3]), int(data[4])])
                #extract transcript ID
                transcript_id = None
                for pair in data[8].split(";"):
                    if pair.strip().startswith("transcript_id"):
                        transcript_id = pair.split()[1].strip('"')
                if transcript_id is None:
                    logging.warning("No transcript id found in line %s" % line)
                    continue
                if transcript_id not in self.transcripts:
                    self.transcripts[transcript_id] = {(chrom, strand, tr_start, tr_end):[]}
                else:
                    self.transcripts[transcript_id][(chrom, strand, tr_start, tr_end)] = []
                    logging.warning("Transcript %s is seen multiple times!" % transcript_id)
            if data[2] == "exon":
                chrom = data[0]
                strand = data[6]
                (ex_start, ex_end) = sorted([int(data[3]), int(data[4])])
                #extract transcript ID
                transcript_id = None
                for pair in data[8].split(";"):
                    if pair.strip().startswith("transcript_id"):
                        transcript_id = pair.split()[1].strip('"')
                if transcript_id is None:
                    logging.warning("No transcript id found in line %s" % line)
                    continue
                if transcript_id not in self.transcripts:
                    logging.warning("transcript id %s no in annotation! Skipping exon %s"
                                    % (transcript_id, line))
                flag = True
                for coord in self.transcripts[transcript_id]:
                    if chrom == coord[0] and strand == coord[1] and ex_start >= coord[2] and ex_end <=coord[3]:
                        self.transcripts[transcript_id][coord].append((ex_start, ex_end))
                        flag = False
                if flag:
                    logging.warning("Exon %s is not assigned to any transcripts" % line)
        for transcript_id in self.transcripts:
            # check if we need this transcript
            if transcript_id not in self.tr2proms:
                continue
            for coord in self.transcripts[transcript_id]:
                transcript = (transcript_id, coord[0], coord[1], coord[2], coord[3], self.transcripts[transcript_id][coord])
                self.add_transcript(transcript)

    
    def add_transcript(self, transcript):
        """ Create refseq transcript from line of refGene file
        and add it to clusters """

        (transcript_id, chrom, strand, tr_start, tr_end, exons) = transcript

        right_proms = self.find_prom(transcript)
        right_proms_num = len(right_proms)
        if right_proms_num == 0:
            return
        elif right_proms_num == 1:
            right_prom = right_proms[0]
        else:
            raise(BaseException(
                "More than one cluster for: %s %s %d %d [%s]\n"
                % (transcript_id,
                   transcript[1],
                   transcript[3],
                   transcript[4],
                   "\t".join(right_proms)
                   )))

        # add exons
        tr_len = sum([(x[1] - x[0] + 1) for x in exons])
        #ex_edges = []
        #for (start, end) in exons:
        #    ex_edges.extend([start, end])
        #tr_start = min(ex_edges)
        #tr_end = max(ex_edges)

        if chrom not in self.exonTree:
            self.exonTree[chrom] = {}
            self.trCluster[chrom] = {}
        if strand not in self.exonTree[chrom]:
            self.exonTree[chrom][strand] = bx.intervals.IntervalTree()
            self.trCluster[chrom][strand] = bx.intervals.cluster.ClusterTree(1, 0)

        # add transcript to transcript clusters
        self.trCluster[chrom][strand].insert(tr_start, tr_end, 0)

        count = 1
        for (estart, eend) in exons:
            # this interval trees search for overlaps inside region
            # excluding start and end so we pad it with 1
            self.exonTree[chrom][strand].add(estart - 1, eend + 1,
                                             (right_prom,
                                              "%s-%d" % (transcript_id, self.tr_count),
                                              tr_len,
                                              count))
            count += 1
        self.tr_count += 1

    def find_prom(self, transcript):
        """ Choose cluster id which contains start position """
        transcript_id = transcript[0]
        chrom = transcript[1]
        strand = transcript[2]

        if strand == '+':
            start = transcript[3]
        else:
            start = transcript[4]

        right_proms = []

        for prom in self.tr2proms[transcript_id]:
            (p_chrom, p_strand, p_start, p_end) = prom.split('_')[2:]

            if p_chrom != chrom:
                continue
            if p_strand != strand:
                continue

            # dirty hack to capture off-by-one error
            if start >= (int(p_start) - 1) and start <= (int(p_end) + 1):
                right_proms.append(prom)

        return right_proms


def multi_open(file_name):
    """ Open file differently depending on file extension """

    if re.search("\.gz$", file_name):
        fin = gzip.open(file_name)
    elif file_name == 'stdin':
        fin = sys.stdin
    else:
        fin = open(file_name)

    return fin


def assign_reads(reads, proms, region):
    (chrom, strand, rstart, rend) = region
    tree = proms.exonTree[chrom][strand]
    for read in reads:
        #if len(read) > 2:
            # logging.debug("chrom: %s strand: %s read: %s %f",
            #               chrom,
            #               strand,
            #               str(read),
            #               reads[read])
        if read[-1] != strand:
            #logging.debug("wrong strand")
            continue
        troverlap = []
        for (start, end) in read[:-1]:
            soverlap = set(tree.find(start, start))
            eoverlap = set(tree.find(end, end))
            data = set.intersection(soverlap, eoverlap)
            #if len(read) > 2:
                # logging.debug("subread start - end: %d - %d" % (start, end))
                # logging.debug("soverlap: %s", str(soverlap))
                # logging.debug("eoverlap: %s", str(eoverlap))
                # logging.debug("data: %s", str(data))
            # exit if empty set
            if not data:
                break
            troverlap.append(set([(x[0], x[1], x[2]) for x in data]))
        # skip read if one of parts not mapped to the same exon
        #if len(read) > 2:
            #logging.debug("troverlap: %s", str(troverlap))
        if len(troverlap) != len(read[:-1]):
            continue

        x = set.intersection(*troverlap)
        tr_num = len(x)
        #if len(read) > 2:
            #logging.debug("tr_num: %d", tr_num)
        for (prom, tr, tr_len) in x:
                proms.counts[prom] += reads[read] / (tr_len * tr_num)


def have_numbers(fin):
    """ Checks if reference sequence are in form of numbers of strings(chr1, chr2,...) 
    need to find better way """
    chroms = [x['SN'] for x in fin.header['SQ'] if x['SN'].startswith('chr')]
    return len(chroms) == 0


def have_numbers_bed(fin):
    """ Checks if reference sequence are in form of numbers or strings in BED """
    chroms = [x for x in fin.contigs if x.startswith('chr')]
    return len(chroms) == 0


###################

if __name__ == '__main__':
    main()
