""" merge reads """

import argparse
import json
import logging
import math
import os
import re
import scipy.stats.mstats
import subprocess


###################

def main():
    """ Main """
    parser = argparse.ArgumentParser(
        description='merge reads')

    parser.add_argument('--bed-dir',
                        dest='bed_dir',
                        default='sorted_bed',
                        help='Directory with bed files')
    parser.add_argument('-d',
                        help='directory with read files',
                        dest='read_dir',
                        default='expr')
    parser.add_argument('-e',
                        dest='expr_file',
                        help='File to save expression.',
                        default='expression.tab')
    parser.add_argument('--lambda',
                        help='Lambda parameter for pseudocount',
                        type=float,
                        dest='lambda_par',
                        default=1.0)
    parser.add_argument('-w',
                        help="Window size. Left and right distance to cover, space separetd, or one number = window width. In last case window is symetric around promoter",
                        dest="window",
                        nargs="+",
                        default=[2000])
    parser.add_argument('-o',
                        help="organism id (hg19 or mm9)",
                        dest="organism",
                        required=True)
    parser.add_argument('--kallisto',
                        help="Flag indicating that data is from kallisto processing",
                        dest="kallisto",
                        action="store_true",
                        default=False)
    args = parser.parse_args()

    genome_length = {'hg19': 3101788170, 'hg18': 3091592211, 'mm9': 2654911517, 'mm10': 2730871774}

    # define left, right window coverage if only one number is given
    if len(args.window) == 1:
        args.window = [int(args.window[0]) / 2, int(args.window[0]) / 2]
    else:
        args.window = [int(x) for x in args.window]
    window_length = sum(args.window)

    read_files = sorted([x for x in os.listdir(args.read_dir)],
                        key=str.lower)
    reads = {}
    bed_files = []
    proms = set()
    for read_file in read_files:
        fin = open(os.path.join(args.read_dir, read_file))
        bed_file = read_file.rstrip('.expr')
        reads[bed_file] = {}
        bed_files.append(bed_file)
        for line in fin:
                entry = line.rstrip()
                if entry == '':
                        continue
                (prom_id, val) = entry.split("\t")
                val = float(val)
                if val == 0.:
                    continue
                reads[bed_file][prom_id] = val
                proms.add(prom_id)

    total_counts = get_total_counts(reads, args.bed_dir, args.kallisto)
    logging.warning("merge_reads_chipseq: total counts %s\n" % str(total_counts))
    fout = open('counts.tab', 'w')
    fout.write("\t".join(bed_files) + "\n")
    fout.write("\t".join([str(total_counts[bed_file])
                          for bed_file in bed_files]) + "\n")
    # write counts table
    for cluster_id in proms:
        counts = []
        for bed_file in bed_files:
            if cluster_id not in reads[bed_file]:
                counts.append(0.0)
                reads[bed_file][cluster_id] = 0.0
            else:
                counts.append(reads[bed_file][cluster_id])

        fout.write("%s\t%s\n" % (cluster_id,
                                 "\t".join([str(x) for x in counts])))
    fout.close()

    table = {}
    for bed_file in bed_files:
        for cluster_id in reads[bed_file]:
            expr = (reads[bed_file][cluster_id]
                    + (total_counts[bed_file] * window_length * args.lambda_par)
                    / genome_length[args.organism])
            if cluster_id not in table:
                table[cluster_id] = []
            table[cluster_id].append(math.log(expr, 2))

    fout = open(args.expr_file, 'w')
    fout.write("\t".join(bed_files) + "\n")

    for cluster_id in table:
        fout.write("%s\t%s\n" % (cluster_id,
                                 "\t".join([str(x) for x in table[cluster_id]])
                                 ))
    fout.close()

    # write sample indices
    fout = open('sample_indices', 'w')
    fout.write("\n".join([re.sub("\.(bam|bed)\.?g?z?$", '', x, flags=2)
                          for x in bed_files]))
    fout.write("\n")
    fout.close()

###################


def get_quantiles(prob, reads):
    """ Get quantile for prob in avery sample in reads """
    quantiles = {}
    for sample in reads:
        quantiles[sample] = scipy.stats.mstats.mquantiles(
            [reads[sample][x] for x in reads[sample]],
            prob=[prob], alphap=0., betap=1.)[0]

    return quantiles


def get_total_counts(reads, bed_dir, is_kallisto):
    """ Returns dict of total counts for samples"""
    total_count = {}

    for sample in reads:
        f = "%s/%s" % (bed_dir, sample)
        count = 0
        if f.lower().endswith(".bam"):
            p = subprocess.Popen(["samtools", "view", "-c", "-F", "4", f],
                                  stdout=subprocess.PIPE)
            out, err = p.communicate()
            count = int(out.rstrip())
        elif f.lower().endswith(".bed.gz"):
            p = subprocess.Popen(["zcat", f], stdout=subprocess.PIPE)
            out = subprocess.check_output(["wc", "-l"], stdin=p.stdout)
            count = int(out.rstrip())
        elif is_kallisto:
            if not os.path.exists("%s/run_info.json" % f):
                raise(BaseException("Could not open file %s/run_info.json" % f))
            with open("%s/run_info.json" % f) as fin:
                count = float(json.loads(fin.read())["n_processed"])
        total_count[sample] = count

    return total_count


###################

if __name__ == '__main__':
    main()
