#!/bin/bash

# needed for enabling access to POSIX environement.
# Set ups PATH, set ups OS X 10.11 permissions
if [ -x /usr/libexec/path_helper ]; then
eval `/usr/libexec/path_helper -s`
fi

if [ "$(uname)" == "Darwin" ]; then
# import system wide bash environment
[[ -f /etc/profile ]] && source /etc/profile

# We should avoid user's specific installation
#[[ -f $HOME/.bash_profile ]] && source $HOME/.bash_profile

fi

# Verifying programs (Python, Perl and R) and file requirements (ISMARA scripts)
err=""
multiple="is" # are
multipleinstall="it" # them

if ! python_ver=$(python -V 2>&1); then

    err="python"

fi

if ! perl_ver=$(perl -e 'print $];'); then


    if [ -n "$err" ]; then
        err+=", "
        multiple="are"
        multipleinstall="them"
    fi

    err+="perl"

fi

if ! r_ver=$(Rscript --version 2>&1); then

    if [ -n "$err" ]; then
        err+=" and "
        multiple="are"
        multipleinstall="them"
    fi

    err+="Rscript"

fi



if [ -n "$err" ]; then
    printf -v omsg '{"type":"%s","severity":%s,"msg":"%s"}' "error" "1" "$err $multiple required but missing. Please install $multipleinstall."
    echo "$omsg" >&2
    exit 1
fi

python_path=$(command -v python 2>&1)
perl_path=$(command -v perl 2>&1)
r_path=$(command -v Rscript 2>&1)

printf -v pmsg '{"type":"%s","severity":%s,"msg":"%s"}' "info" "1" "$python_ver ($python_path)<br>perl $perl_ver ($perl_path)<br>$r_ver ($r_path)"
echo "$pmsg"

export ROOT=$(dirname $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ))

FILES=( chipseq_readRegion.py chipseq.sh common_utils.sh env.sh merge_reads_chipseq.py merge_reads.py microarray.sh process_affy_files.R quantile_normalization.R readRegion.py rnaseq.sh )

for FILE in "${FILES[@]}"
do

    if [ ! -f "$ROOT/scripts/"$FILE ]; then

        printf -v omsg '{"type":"%s","severity":%s,"msg":"%s"}' "error" "1" "$ROOT/scripts/$FILE was missing, plase make sure installation is complete."
        echo "$omsg" >&2
        exit 1

    fi

done
