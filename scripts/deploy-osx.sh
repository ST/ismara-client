#!/bin/bash

MACROOT=$(dirname "$(pwd)")
USER=`whoami`

# get version and paths
echo " "
VTAG=$(git describe --tag)
read -p "Version tag ($VTAG): " answer;
if [[ -z "$answer" ]]
then
echo "Version tag is required, sorry!"; exit
else
VERSION=$answer
fi
echo " "

read -p "Absolute path to the QT5-macdeployedqt installation:
(leave empty for default: /Users/$USER/QT5/5.4/clang_64/bin)" answer1;
if [[ -z "$answer1" ]]
then
export MACDEPLOYQT=/Users/$USER/Qt/5.4/clang_64/bin
else
export MACDEPLOYQT=$answer1
fi
echo "MACDEPLOYQT: $MACDEPLOYQT"
echo " "

read -p "Absolute path to the ismara-client (build path):
(leave empty for default: $MACROOT/ismara-client)" answer2;
if [[ -z "$answer2" ]]
then
export ISMARA_BUILD_PATH=$MACROOT/ismara-client
else
export ISMARA_BUILD_PATH=$answer2
fi
echo "ISMARA_BUILD_PATH: $ISMARA_BUILD_PATH"
echo " "

read -p "Absolute path to the deployed Ismara client:
(leave empty for default: $MACROOT/release)" answer3;
if [[ -z "$answer3" ]]
then
export ISMARA_INSTALL_PATH=$MACROOT/release
else
export ISMARA_INSTALL_PATH="$answer3"
fi
echo "ISMARA_INSTALL_PATH: $ISMARA_INSTALL_PATH"
echo " "

# Ismara.app bundle
mkdir $MACROOT/release/Ismara.app/Contents/MacOS/logs
cp -r $MACROOT/ismara-client/scripts $MACROOT/release/Ismara.app/Contents/MacOS/
rm -rf $MACROOT/release/Ismara.app/Contents/MacOS/scripts/R_libs/linux
rm -rf $MACROOT/release/Ismara.app/Contents/MacOS/scripts/python_libs/linux
rm $MACROOT/release/Ismara.app/Contents/MacOS/scripts/deploy-*
cp -r $MACROOT/ismara-client/utils $MACROOT/release/Ismara.app/Contents/MacOS/
rm -rf $MACROOT/release/Ismara.app/Contents/MacOS/utils/linux
cp -r $MACROOT/ismara-client/doc $MACROOT/release/Ismara.app/Contents/MacOS/doc

# edit the Info.plist by adding version tag + git release
cp $MACROOT/ismara-client/Info.plist $MACROOT/release/Ismara.app/Contents/
cd $MACROOT/ismara-client; YYY=$(git describe --tag); cd ..;
perl -pi -e "s/XXX/$VERSION/g" $MACROOT/release/Ismara.app/Contents/Info.plist
perl -pi -e "s/YYY/$YYY/g" $MACROOT/release/Ismara.app/Contents/Info.plist

# The input data is available in
# https://rxtx.isb-sib.ch/public.php?service=files&t=e4ae2e9a50749575c0e4e693b70b2f55
# or

scp -r $USER@frt.vital-it.ch:/archive/sib/sib_tech/projects/ismara/data $MACROOT/release/Ismara.app/Contents/MacOS/

cd $MACROOT/release
$MACDEPLOYQT/macdeployqt Ismara.app -qmldir=$MACROOT/ismara-client/ -always-overwrite -verbose=1

cd $MACROOT
mkdir ISMARA
cp -R release/Ismara.app ISMARA/

cd $MACROOT/ISMARA/Ismara.app/Contents/MacOS/scripts/
### remove all '# @Internal:' comments from *.sh scripts
for file in `ls *sh`; do egrep -v '^# @Internal: ' $file > $file.new; mv $file.new $file; done
chmod a+x *.sh

# => move following directories from "MacOS" to "Resources"
cd $MACROOT/ISMARA/Ismara.app/Contents/Resources/
mv ../MacOS/logs .
mv ../MacOS/doc .
mv ../MacOS/data .
mv ../MacOS/scripts .

# => create a symlink from "MacOS" to "Resources" (to keep compatibility with Linux structure)
cd $MACROOT/ISMARA/Ismara.app/Contents/MacOS/
ln -s ../Resources/logs logs
ln -s ../Resources/doc doc
ln -s ../Resources/data data
ln -s ../Resources/scripts scripts

cd $MACROOT/ISMARA/Ismara.app/Contents/Resources/scripts/R_libs
ls osx > tmp.txt
perl -e 'while(<>){if(/(\S+)/){print "ln -s osx\/$1 $1\n";}}exit;' tmp.txt > tmp-cmds.txt
source tmp-cmds.txt
rm tmp.txt tmp-cmds.txt

cd $MACROOT/ISMARA
# create the ReadMe.txt ... 
cat >> ReadMe.txt <<EOD
Ismara.app needs to be copied on the Mac before it can be used.
We suggest you drag'n'drop the Ismara icon into your application folder.
EOD
rm -f .DS_Store
touch .Trash

# create the image disk ...
cd ../
hdiutil create -volname ISMARA-INSTALLER -srcfolder ISMARA -ov -format UDZO "ISMARA-$VERSION.dmg"

echo "All ok, terminating"
