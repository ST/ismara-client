#!/bin/bash

# Access to POSIX environement.
# Set ups PATH, set ups OS X 10.11 permissions
if [ -x /usr/libexec/path_helper ]; then
eval `/usr/libexec/path_helper -s`
fi

# Trap the error code and fail when the exit code is not 0
# for one of the steps
function exit_error {
    echo "$1" 1>&2 >> $LOG
    echo "$0: ERROR! Check the log file: $LOG"
    exit 1
}

# errortype, severity, message, log-true/false exit_code (exit if set)
function json_out {
    etype=$1
    severity=$2
    msg=$3

    msg=${msg////\\\/}

    printf -v omsg '{"type":"%s","severity":%s,"msg":"%s"}' "$etype" "$severity" "$msg"
    if [ "$4" == "true" ]
    then
    echo `date +"%Y-%m-%d %T"` "$etype $3">>$LOG
    fi

    if [ ! -z "$5" ]
    then
    sleep 1
    echo "$omsg" >&2
    exit $5
    fi
    echo "#${omsg}#"

    # if we output too fast qml sees more than 1 json string concatenated together
    #sleep 1
}

check-input-files () {
	array=$1
	for (( i = 4 ; i < ${#array[@]} ; i++ ))
	do
		file1="${array[$i]//___/ }"
		for (( j=4 ; j < ${#array[@]} ; j++ ))
		do
			if [ "$j" != "$i" ]; then
				file2="${array[$j]//___/ }"
				if diff "$file1" "$file2" > /dev/null; then
					json_out "error" 2 "$file1 and $file2 are duplicates" true 1
				fi
			fi
		done
	done
}

# @Internal: Handle cases where the file path
# @Internal: contains spaces
# @Internal: Spaces have been replaced by 3 underscores in QML
# @Internal: Replace those 3 underscores by spaces
# Because FILENAME may contain spaces, the variable sould be
# double-quoted in command lines.
format-and-index() {

	FILEPATH=$1
	FILE=$2
 
	FILEPATH=${FILEPATH//___/ }

	bam=$(echo "$FILE" | egrep -i "\.bam(\.[a-z\d]+)?$")
	bed=$(echo "$FILE" | egrep -i "\.bed(\.[a-z\d]+)?$")
	sam=$(echo "$FILE" | egrep -i "\.sam(\.[a-z\d]+)?$")

	if [ -n "$bam" ]; then 
		format-bam-and-do-bai-index "$FILEPATH" $FILE 
	elif [ -n "$bed" ]; then
		format-bed-and-do-tbi-index "$FILEPATH" $FILE
	elif [ -n "$sam" ]; then
		format-and-index-sam "$FILEPATH" $FILE
	else
		json_out "error" 1 "Bad file format: $FILE" true
	fi
}

# Run summarizeExpression.pl and create the resulting archive
prepare_output (){
	data_type=$1	
	arguments=""

	if  [ "$data_type" == "microarray" ]; then
		arguments="--expr $TMP/expression.tab --sites $INPUT/$genome/siteCounts/$micro/sitecount_matrix_allProms --probes $INPUT/probe2cluster.txt --type microarray --out $TMP"
	else
		arguments="--expr $TMP/expression/expression.tab --sites $INPUT/$genome/siteCounts/$micro/sitecount_matrix_allProms --type $data_type --out $TMP"
	fi

	summarizeExpre_Cmd="$IPATH/summarizeExpression.pl $arguments"
	json_out "info" 1 "Running summarizeExpression:" true

    json_out "progress" 1 1 false

	perl $summarizeExpre_Cmd || json_out "error" 1 "perl $summarizeExpre_Cmd failed!" true 1 
	json_out "info" 1 "End summarizeExpression:" true

	json_out "info" 1 "Creating the archive for the downstream ISMARA job" true

	cd $TMP
	mkdir -p $TMP/results
	[[ ! -d "$TMP/results" ]] && json_out "error" 1 "Can't create directory $TMP/results" true 1
	[[ ! -w "$TMP/results" ]] && json_out "error" 1 "Can't write to directory $TMP/results" true 1

    if [ "$data_type" == "microarray" ]; then
		mv sample_indices results || json_out "error" 1 "Cannot move sample_indices" true 1
	else
		mv expression/sample_indices results || json_out "error" 1 "Cannot move sample_indices" true 1
	fi

    json_out "progress" 1 1 false

	mv prom_indices results || json_out "error" 1 "Cannot move prom_indices" true 1
	mv sitecount_mat results || json_out "error" 1 "Cannot move sitecount_mat" true 1
	mv prom_expression results || json_out "error" 1 "Cannot move prom_expression" true 1
	mv mat_indices results || json_out "error" 1 "Cannot move mat_indices" true 1

	tar -zcvf results-$JOBID.tar.gz results || json_out "error" 1 "Cannot create the result archive" true 1
	cp results-$JOBID.tar.gz $OUTPUT || json_out "error" 1 "Cannot copy results-$JOBID.tar.gz" true 1
}

format-and-index-sam() {
	FILEPATH=$1
	FILE=$2

	json_out "info" 1 "Converting $FILE SAM file to BAM" true

	samtools view -bSh "$FILEPATH" >$TMP/processing/$FILE.bam || json_out "error" 1 "samtools view -bSh $FILEPATH >$TMP/processing/$FILE.bam" true $?
	format-bam-and-do-bai-index $TMP/processing/$FILE.bam $FILE.bam
}

format-bam-and-do-bai-index() {

    FILEPATH=$1
    FILE=$2

    json_out "info" 1 "Sorting user's input file (BAM format)" true
    samtools sort "$FILEPATH" $TMP/processing/$FILE.sorted || json_out "error" 1 "samtools sort $FILEPATH $TMP/processing/$FILE.sorted failed" true $?

# @Internal: Renaming: sort adds a '.bam' extension to the resulting file!
    mv $TMP/processing/$FILE.sorted.bam $TMP/processing/$FILE || json_out "error" 1 "mv $TMP/processing/$FILE.sorted.bam $TMP/processing/$FILE failed" true $?
	json_out "info" 1 "End sorting of BAM file" true

    samtools index $TMP/processing/$FILE || json_out "error" 1 "samtools index $TMP/processing/$FILE failed" true $?
	json_out "info" 1 "End indexing of BAM file $FILE" true
}

format-bed-and-do-tbi-index() {

	FILEPATH=$1
	FILE=$2

    json_out "info" 1 "Sorting user's input file $FILE..." true

    # Test for progress indicator
    json_out "progress" 1 1 false
    # Sort files and then create tbi index files with tabix
    gzip -d -c "$FILEPATH" | sort-bed --max-mem "$mem_limit" - | bgzip -c > $TMP/processing/$FILE.sort
    retval_0="${PIPESTATUS[0]}" retval_1="${PIPESTATUS[1]}" retval_final=$?

    if [ $retval_0 -ne 0 ]
    then
        json_out "error" 1 "tbi index and sorting failed: gzip -d -c $FILEPATH unsuccessful ($retval_0)" true $retval_0
    elif [ $retval_1 -ne 0 ]
    then
        json_out "error" 1 "sort-bed --max-mem "$mem_limit" - unsuccessful ($retval_1)" true $retval_1
    elif [ $retval_final -ne 0 ]
    then
        json_out "error" 1 "tbi index and sorting failed: bgzip -c > $TMP/processing/$FILE.sort unsuccessful (retval_final)" true retval_final
    fi

    json_out "info" 1 "End sorting $FILE" true

    json_out "info" 1 "Renaming sorted file $FILE" true

    mv $TMP/processing/$FILE.sort $TMP/processing/$FILE || json_out "error" 1 "Problem when renaming $FILE.sort" true $?

    json_out "info" 1 "Indexing $FILE using tabix" true
    json_out "progress" 1 1 false
    tabix -p bed $TMP/processing/$FILE || json_out "error" 1 "tabix -p bed $TMP/processing/$FILE failed" true $?

    json_out "info" 1 "End indexing $FILE" true
}

