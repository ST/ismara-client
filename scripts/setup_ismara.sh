#!/bin/bash

work=$1
build=$2

if [ "$(uname)" == "Darwin" ]; then
	build=$build/Ismara.app/Contents/MacOS
fi

cd $build
cp -r $work/scripts .
cp -r $work/utils .
mkdir -p logs tmp

# @Internal: Cannot scp the data dir from archive
# @Internal: because the QtCreator's "Custom build step"
# @Internal: processor, does not seem to tolerate it.
# @Internal: Anyway, getting the genome data at each run is an overkill!
