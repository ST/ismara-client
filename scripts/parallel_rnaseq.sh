#!/bin/bash

export ROOT=$(dirname $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ))

source $ROOT/scripts/common_utils.sh

FILEPATH=$1
FILE=$(basename $FILEPATH)

#json_out "info" 1 "Formatting and creating index for file: $FILE" true

format-and-index $FILEPATH $FILE

# hardcoded number of this step
json_out "info" 2 "STEP 3: Reading regions" true

if [ "$genome" = "hg19" -o "$genome" = "mm10" ]; then
    t_option="$INPUT/$genome/transcript_annotation.gtf.gz"
else 
    t_option="$INPUT/$genome/refSeqAli.gz $INPUT/$genome/all_mrna.gz"
fi

readRegion_Cmd1="$IPATH/readRegion.py -t $t_option -p $INPUT/$genome/clusters_150_gene_associations.gz -i $TMP/processing/$FILE --expr-dir $TMP/expression"
python $readRegion_Cmd1 || json_out "error" 1 "Script $0 Line $LINENO: Problem with: python $readRegion_Cmd1 Error: $( { python $readRegion_Cmd1; } )" true $?

