#!/bin/bash

# Set env variables for ISMARA pre-processing
# ROOT is set in the upstream script
script=$1

if [ "$(uname)" == "Darwin" ]; then
    export OS="osx"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    export OS="linux"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    export OS="win"
fi


# @Internal: . "${HOME}/.config/sib/ismara.conf"

IFS='@' read -r logPath pythonPath rPath dataOutput tmpPath <<< "$2"
unset IFS

# 1. Uniquely identify a given job (for now we use timestamp)
# @Internal: There may be more specific (and safer) ways
# 2. Print everything in a log file
if [ "${logPath: -1}" != "/" ];then
logPath="${logPath}/"
fi

# if we have absolute path don't add ROOT
if [ "${logPath:0:1}" != "/" ]
then
logPath="$ROOT/${logPath}"
fi

mkdir -p "$logPath"
[[ ! -d "$logPath" ]] && json_out "error" 1 "Can't create directory "$logPath true 1
[[ ! -w "$logPath" ]] && json_out "error" 1 "Can't write to directory "$logPath true 1

export LOG=${logPath}$JOBID.log


json_out "info" 1 "LOG dir: "$LOG true

# @Internal: Details:
# @Internal: I got the 2 following packages from:
# @Internal: https://pypi.python.org/packages/source/p/pysam/pysam-0.8.2.1.tar.gz#md5=daf63c1665fa601fc942f050f334a627
# @Internal: https://bitbucket.org/james_taylor/bx-python/get/tip.tar.bz2
# @Internal: unpacked them and did the following:
# @Internal: python setup.py build
# @Internal: python setup.py install --root=/Users/sduvaud/ISMARA/ismara-qml/scripts/python_libs
# @Internal: Issues/Questions:
# @Internal: Do we package those in the ISMARA app?
# @Internal: Should we change the prefix in order to have a prettier PYTHONPATH variable?
# @Internal: Should we keep the libs outside of the ISMARA code?
# @Internal: How do we manage the update of libraries?
# @Internal: How to make sure that our python scripts will be compatible with the libs in the long run?
# @Internal: Do you keep the distribution in git?
# @Internal: Licensing issues to be figured out!

# Python libraries
if [ "${pythonPath: -1}" != "/" ]; then
pythonPath="${pythonPath}/"
fi

# if we have absolute path don't add ROOT
if [ "${pythonPath:0:1}" != "/" ];
then
export pythonPath="$ROOT/${pythonPath}"
fi

# Separated libraries for linux and OSX
if [ $OS == "osx" ]; then
export PYTHONPATH=${pythonPath}/"$OS"
else
export PYTHONPATH=${pythonPath}$PYTHONPATH:$ROOT/scripts/python_libs/$OS/Library/Python/2.7/dist-packages:$ROOT/scripts/python_libs/$OS/Library/Python/2.7/site-packages:$ROOT/scripts/python_libs/$OS/usr
fi

json_out "info" 1 "pythonPath: "$PYTHONPATH true

if [ "${rPath: -1}" != "/" ];then

rPath="${rPath}/"

fi

# if we have absolute path don't add ROOT
if [ "${rPath:0:1}" != "/" ];
then
export rPath="$ROOT/${rPath}"
fi

# R libraries
# Separated libraries for linux and OSX
export R_LIBS=${rPath}:$ROOT/scripts/R_libs/$OS

json_out "info" 1 "R_LIBS: "$R_LIBS true

# Contains required utilities such as bed-sort, bgzip, ...
# @Internal: Details:
# @Internal: I got the 2 following packages from github ($TMP directory):
# @Internal: git clone https://github.com/samtools/htslib.git
# @Internal: git clone git://github.com/samtools/samtools.git
# @Internal: git clone https://github.com/bedops/bedops.git
# @Internal: and installed them locally using the following command lines:
# @Internal: make DESTDIR=/Users/sduvaud/ISMARA/utils
# @Internal: make DESTDIR=/Users/sduvaud/ISMARA/utils install
# @Internal: (make and make install for bedops)
# @Internal: all the binaries were copied to $UTILS
# @Internal: Licensing issues to be figured out!
export UTILS=$ROOT/utils/$OS
export PATH=$PATH:$UTILS

# Location of 4 input files:
# all_mrna  clusters_150_gene_associations  refSeqAli  sitecount_matrix_allProms
# @Internal: Same questions as above for the libraries
export INPUT=$ROOT/data/src
json_out "info" 1 "INPUT dir: "$INPUT true

# Location of the files requested for the ISMARA web service
# (Downstream job)
if [ "${dataOutput: -1}" != "/" ];then

dataOutput="${dataOutput}/"

fi

# if we have absolute path don't add ROOT
if [ "${dataOutput:0:1}" == "/" ];
then
export OUTPUT=${dataOutput}
else
export OUTPUT=$ROOT/${dataOutput}
fi

json_out "info" 1 "OUTPUT dir: "$OUTPUT true

# Location of processing scripts from Mikhail Pachkov
export IPATH=$ROOT/scripts

# Location of temporary files
# @Internal: This part overwrites what was already implemented (I removed the part)
# @Internal: At that time I decided to rm -rf the directory TMP/processing and TMP/expression
# @Internal: because the ouput dir is always named result and the archive result.tar.gz
# @Internal: We should keep the structure of the TMP file only in case we find a way to uniquely
# @Internal: name the output files
# @Internal: and a TMP directory is meant to be erased!
# @Internal: using system provided temp path which doesn't have the ending slash

if [ "${tmpPath: -1}" != "/" ];then

tmpPath=${tmpPath}/

fi
export TMP="${tmpPath}$JOBID"
json_out "info" 1 "TMP dir: "$TMP true
