#!/bin/bash

export ROOT=$(dirname $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ))

source $ROOT/scripts/common_utils.sh

array=( $@ )
len=${#array[@]}

check-input-files array || json_out "error" 1 "Input file error" true 1

# Retrieve the script name from the script's base name
programName=$(basename "$0" | cut -d. -f1) 

# Genome (mm9, hg19, hg18)
# @Internal: For now no check.
# @Internal: Maybe we should check:
# @Internal: whether the var is empty
# @Internal: whether it is equal to mm9, hg19 or hg18
# @Internal: as the script will be run within ISMARA, it may be an overkill
# @Internal: Check the scenarios (depending on what we decide for the processing of data)
genome=$1

# Possibility to choose between sitecount matrix with/without microRNAs.
# @Internal: Corresponding sitecount matrices are:
# @Internal: siteCounts/SwissReg_TargetScan/sitecount_matrix_allProms
# @Internal: ==> Include miRNA
# @Internal: siteCounts/SwissReg/sitecount_matrix_allProms
# @Internal: ==> Do not include miRNA
micro=$2

# JOBID
JOBID=$3

# get paths
PATHS=$4
source $ROOT/scripts/env.sh $programName $PATHS


# step X/Y
steps_outside_file_loop=3

# progress bar, or cost of operation "progress 4" all summed up outside loops
items_outside_file_loop=7

# Number of steps (progress bar) for one file
items_for_files=0

# number of files
number_of_files=$((len - 3))

# progress length of recurring file operations
items_inside_file_loop_sum=$((number_of_files * items_for_files))

# how many steps per file in a loop # step X/Y of file 1/z
steps_for_files_in_loop=0

# steps for files inside loop summed
steps_for_files_sum=$((steps_for_files_in_loop * number_of_files))

# how many progress steps in total
steps_total=$((steps_outside_file_loop + steps_for_files_sum))

# total of reported steps
progress_steps_main_total=$((steps_for_files_sum + progress_functions_exluding_per_file - progress_steps_for_files_in_loop))

items_total=$((items_outside_file_loop + items_inside_file_loop_sum))

json_out "progress_total" 1 $items_total false

filestep=1

# list of files
space=" "
for (( i = 4 ; i < ${#array[@]} ; i++ ))
do
# @Internal: In QML, we replaced the spaces in paths
# @Internal: by 3 underscores.
# @Internal: Replacing back the 3 underscores by a space
# @Internal: is handled by the R wrapper script
# @Internal: because R was unable to deal with spaces in path
	path=${array[$i]}
	list=$list$space$path
done
list="${list%"${list##*[![:space:]]}"}"   # remove trailing whitespace characters

json_out "info" 1 "root is set to $ROOT" true

# we have 5 parameters, and filelist should have 2 (=6) anything less and we miss something
if [ $len -lt 6 ]; then
    json_out "error" 1 "Error: At least 2 CEL files are required!" true 1
fi

# @Internal: use severity 2 for progress that shows up in progress bar
# @Internal: first step, create directories, cost 1
json_out "info" 2 "STEP $filestep/$steps_total: Creating job directories" false

filestep=$((filestep + 1))

mkdir -p $OUTPUT
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't create directory $OUTPUT" true 1
[[ ! -w "$OUTPUT" ]] && json_out "error" 1 "Can't write to directory $OUTPUT" true 1

mkdir -p $TMP
[[ ! -d "$TMP" ]] && json_out "error" 1 "Can't create directory $TMP" true 1
[[ ! -w "$TMP" ]] && json_out "error" 1 "Can't write to directory $TMP" true 1

json_out "progress" 2 1 false

json_out "info" 2 "STEP $filestep/$steps_total: Processing CEL files" false
filestep=$((filestep + 1))

json_out "info" 1 "==> Running Rscript microarrayProcessing.R $list $TMP" true

Rscript $IPATH/microarrayProcessing.R $list $TMP || json_out "error" 1 "Problem with microarrayProcessing.R" true 1
json_out "progress" 2 4 false

json_out "info" 1 "R script done" true

json_out "info" 2 "STEP $filestep/$steps_total: Preparing output" false

mv $TMP $OUTPUT || json_out "error" 1 "Cannot move temp directory to $OUTPUT" true 1
 
json_out "info" 1 "END workflow" true
json_out "info" 1 "Final result is available in $OUTPUT/$JOBID/expression*.tab and will be uploaded to ISMARA" true

chipID=`ls $OUTPUT/$JOBID`
json_out "chip_id" 1 "$chipID" true
json_out "success" 1 1 false
