#include "httpuploader_plugin.h"
#include "httpuploader.h"

#ifdef QT5_BUILD
#include <QtQml/qqml.h>
#else
#include <QtDeclarative/qdeclarative.h>
QML_DECLARE_TYPE(HttpUploader)
#endif

void HttpUploaderPlugin::registerTypes(const char *uri)
{
    // @uri HttpUploader
    qmlRegisterType<HttpUploader>(uri, 0, 1, "HttpUploader");
}

#ifndef QT5_BUILD
Q_EXPORT_PLUGIN2(HttpUploader, HttpUploaderPlugin)
#endif

