TEMPLATE = lib
TARGET = httpuploader

DEFINES += QT5_BUILD
QT += qml

CONFIG += qt plugin

TARGET = $$qtLibraryTarget($$TARGET)
uri = HttpUploader

# Input
SOURCES += \
    httpuploader_plugin.cpp \
    httpuploader.cpp

HEADERS += \
    httpuploader_plugin.h \
    httpuploader.h

OTHER_FILES = qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) \"$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)\"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir


    installPath = ../plugin/$$replace(uri, \\., /)

    qmldir.path = $$installPath
    target.path = $$installPath
    INSTALLS += target qmldir

