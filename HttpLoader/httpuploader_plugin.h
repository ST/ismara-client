#ifndef HTTPUPLOADER_PLUGIN_H
#define HTTPUPLOADER_PLUGIN_H

#ifdef QT5_BUILD

#include <QtQml/QQmlExtensionPlugin>

class HttpUploaderPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

#else // QT4

#include <QtDeclarative/QDeclarativeExtensionPlugin>

class HttpUploaderPlugin : public QDeclarativeExtensionPlugin
{
    Q_OBJECT

#endif

public:
    void registerTypes(const char *uri);
};

#endif // HTTPUPLOADER_PLUGIN_H

