import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import "component-utils.js" as Utils

Row {
    // id
    id: optionRow
    spacing: 10

    SlidingMenu { }

    Rectangle {
        id: background

        color: "#f5f5f5"

        border.color: "lightgrey"
        border.width: 1
        radius: 5

        width: 450
        height: 140

        ListView {
            id: fileChooser
            objectName: "fileChooser"

            //width: parent.width
            //height: parent.height

            anchors.margins: 6
            anchors.fill: parent

            model: fileChooserModel
            delegate: fileChooserDelegate

            clip: true
        }

        Component {
            id: fileChooserDelegate

            Row {
                spacing: 5

                Button {
                    id: deleteIcon
                    visible: deleteFlag
                    iconSource: "img/delete.png"
                    tooltip: "Remove file"

                    width: 16
                    height: 16

                    style: ButtonStyle {
                        background: Rectangle {
                            color: "#f5f5f5"
                            opacity: 0
                        }
                    }
                    onClicked: Utils.remove_file(index)

                }
                Text { text: files.replace(/___/g," ") ; elide: Text.ElideMiddle; width: 400;}
            }

        }

        ListModel {
            id: fileChooserModel
            objectName: "fileChooserModel"

            ListElement {
                deleteFlag: false
                files: "Please, import your data"
            }
        }

        Button {
            id: addButton
            text: "Add"
            // iconSource: progName == 'microarray' ? "img/folder_add.png" : "img/file_add.png"
            //iconSource: "img/file_add.png"
            //iconSource: "img/add.png"
            iconSource: "img/add_small.png"

            anchors.top: parent.bottom
            anchors.right: parent.right
            anchors.topMargin: 3

            onClicked: {
                uploadOK = true
                resultText = "" // clear result line
                openFileDialog()
            }

            function openFileDialog()
            {
                if (progName == 'microarray')
                {
                    fileDialog.setSelectFolder(false);
                    fileDialog.setSelectMultiple(true);
                    // CEL files only
                    // if for whatever reason, the user renamed his/her files
                    // then he/she should be given the possibility to view all the files
                    // This implies checks in the upcoming steps
                    fileDialog.setNameFilters(["CEL files (*.cel* *.CEL*)"]);
                    fileDialog.selectedNameFilterIndex = 0;
                }
                else
                {
                    fileDialog.setSelectFolder(false);
                    fileDialog.setSelectMultiple(true);
                    // BED, BAM and SAM files only
                    // if for whatever reason, the user renamed his/her files
                    // then he/she should be given the possibility to view all the files
                    // This implies checks in the upcoming steps
                    fileDialog.setNameFilters(["BED, BAM, SAM files (*.bed* *.bam* *.sam* *.BED* *.BAM* *.SAM* *.fastq* *.FASTQ*)"]);
                    fileDialog.selectedNameFilterIndex = 0;
                }
                fileDialog.open();
            }

        }

        Button {
            id: clearButton
            objectName: "clearButton"

            text: "Clear"

            anchors.top: parent.bottom
            anchors.right: addButton.left
            anchors.rightMargin: 3
            anchors.topMargin: 3

            onClicked: Utils.clear_selected_files();
        }

        Text {
            id: nbFiles
            text: qsTr("")
            anchors.verticalCenter: clearButton.verticalCenter
            anchors.left: background.left
            anchors.leftMargin: 6

        }
    }

    FileDialog {
        // id
        id: fileDialog

        // event handling
        onAccepted: {

            var list = "";
            var listAsArray = "";

            // if the user already selected files
            if (fileList.trim() !== "")
            {
                list = fileList.trim();
                listAsArray = list.split(" ");
            }

            var fileOrDir = "d";
            var first = fileUrls[0].toLowerCase().split('.').pop();
            if (first === "gz" || first === "bam" || first === "bed" || first === "sam")
                fileOrDir = "f";

            var fileCount = fileUrls.length + listAsArray.length;
            fileCount > 1 ? parameter.processEnabled = true : parameter.processEnabled = false;

            if (fileOrDir === "f")
            {
                var s = fileCount > 1 ? "s":"";
                nbFiles.text = fileCount + " file" + s + " selected";
            }
            else
                nbFiles.text = "One directory selected"; // By the way, do we allow 1+ dirs?

            // Append the list of files to the last index
            // Test with re-running of data processing!
            var lastFileChooserIndex = fileChooserModel.count;

           // Reset the first element and change the wording:
            fileChooserModel.remove(0);
            var label = fileOrDir === "f" ? "Files to be processed:":"Directory:"
            fileChooserModel.insert(0, {"files": "<b>" + label + "</b>"});

            for (var i = 0; i < fileUrls.length; ++i)
            {
                var shortFileName = fileUrls[i].replace("file://","").replace(/ /g,"___");
                fileChooserModel.insert(lastFileChooserIndex, {"deleteFlag": true, "files": shortFileName});
                lastFileChooserIndex++;

                list += " ";
                list += shortFileName;
            }

            fileList = list.trim();
        }
        onRejected: { }
    }
}
