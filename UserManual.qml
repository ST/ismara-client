import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Window {

    id: userManual
    x: 100
    y: 60

    width: 750
    height: 700

    title: app.programTitle+" Manual"

    Rectangle {
        id: userManualContainer
        anchors.fill: parent


        width: parent.width
        height: parent.height


        ScrollView{
            height: parent.height
            width: parent.width



            Flickable {
                id: userManualFlickableContent
                anchors.fill: parent
                height: userManual.height
                width: userManual.width
                contentWidth: userManualText.width
                contentHeight: userManualText.height + 20
                //flickableDirection: Flickable.HorizontalAndVerticalFlick
                //flickDeceleration: 5000

                // Flickable does not automatically clip its contents. If it is not used as a full-screen item,
                // you should consider setting the clip property to true.
                clip: true

                Rectangle {

                    TextEdit {
                        id: userManualText
                        width: userManualContainer.width - 30
                        anchors.margins: 20
                        // This setting makes the text wrap at word boundaries when it goes past the width of the Text object
                        wrapMode: Text.WordWrap
                        readOnly: true

                        x:10
                        y:10

                        text: "ISMARA (Integrated System for Motif Actitivity Response Analysis) is an online tool that models genome-wide expression or ChIP-seq data, in terms of computationally predicted regulatory sites for transcription factors (TFs) and micro-RNAS (miRNAs).

The input required for running ISMARA is either expression data (microarray or RNA-seq data), or ChIP-seq data from a set of biological samples.
Developed by the Genome Systems Biology (SIB Swiss Institute of Bioinformatics), ISMARA is a valuable tool, which has been successfully used by numerous scientists worldwide.
However, there is a potential problem  with the ISMARA online tool since the input files can be very big (several GBs) and thus their upload to the server could require a lot of time depending on the user's network connection speed and reliability.

ISMARA client: Why?

ISMARA client is a standalone application developed by the SIB Swiss Institute of Bioinformatics, which can be installed on a user's computer in order to locally pre-process expression or ChIP-seq data. The pre-processed data is significantly smaller in size and can be uploaded in reasonable time even with slow Internet connection speed.
The resulting file (several MBs) is a simple tab-delimited file.

The format is the following:

- The first row contains the sample names.
- The subsequent rows contain a unique identifier and, for each sample, a value representing the abundance of mRNA (microarray, RNA-seq) or the abundance of sequence bound to specific protein (ChIP-seq).

Examples:

1. microarray:

microarray-analysis microarray-analysis-2
1007_s_at:592:319 6.16165957114708 7.06761567764624
1007_s_at:85:591 3.75901253140705 2.06978028378736
1007_s_at:559:139 7.63943535140124 6.67261363470392

Column #1 consists of the probe identifier.
Columns #2 and #3 consist of the mRNA abundance in sample microarray-analysis and the mRNA abundance in sample microarray-analysis-2, respectively. In microarray analysis, the abundance represents the intensity of a spot on the chip.

2. RNA-seq:

rna-seq-analysis.bam rna-seq-analysis-2.bam
hg19_v1_chr1_-_21377452_21377481 7.99467062941 7.99467062941
hg19_v1_chr1_+_161195832_161195883 16.4752193881 16.4752193881
hg19_v1_chr1_-_55681024_55681048 9.47673322255 9.47673322255

Column #1 is an identifier. It is composed of:

- a version number of promoter annotation,
- the chromosome number
- the strand
- the positions of the promoter on the chromosome separated by underscores.

Columns #2 and #3 consist of the mRNA abundance in rna-seq-analysis.bam and the mRNA abundance in sample rna-seq-analysis-2.bam, respectively. In RNA-seq analysis, the abundance represents the number of reads mapping to a region of interest.

3. ChIP-seq:

chipseq-analysis.bam chipseq-analysis-2.bam
hg19_v1_chr1_-_21377452_21377481 0.139883459941 0.139883459941
hg19_v1_chr1_+_32479401_32479596 0.139883459941 0.139883459941</td>
hg19_v1_chr1_+_183155173_183155211 0.343104307856 0.343104307856

Column #1 is an identifier. It is composed of:

- a version number of promoter annotation,
- the chromosome number
- the strand
- the positions of the promoter on the chromosome separated by underscores.

Columns #2 and #3 consist of the abundance of sequence bound to specific protein in sample chipseq-analysis.bam and the abundance of sequence bound to specific protein in sample chipseq-analysis-2.bam, respectively.In ChIP-seq analysis, the abundance represents the number of reads mapping to a region of interest.

The tab-delimited file is automatically uploaded to the ISMARA server in order to be further processed. Thus, ISMARA users save themselves the annoyance of the uploading of very big files.

ISMARA client: How?

In order to ease the user experience, the user interface of the ISMARA-client application follows the ISMARA web site's look and feel.
Indeed, the user starts by selecting the data type (microarray, RNA-seq or ChIP-seq). For RNA-seq and ChIP-seq, the user is also requested to select a genome assembly (human genome versions 18 and 19 (hg18, hg19) or mouse genome version 9 (mm9)).
By default, if the user selects microarray or RNA-seq, mi-RNA are used to run the analysis. The default behavior can of course be changed.

Once the options of the pipeline are chosen, the user adds some files (2 files minimum required, in compressed format or not):

- CEL files for microarray
- BAM, SAM or BED for RNA-seq and ChIP-seq
and starts the pre-processing.

In ISMARA-client, like on the ISMARA web site, it is recommended (although not mandatory) to specify a project name. It is also a good practice to provide an email address to be notified when the ISMARA sever has finished analyzing the data.

In ISMARA-client, all the jobs (i.e. individual requests) are stored in a table, with their status and a link to the corresponding ISMARA reports (which prevents the user from bookmarking each ISMARA result page). A detailed log information is also available and can be copy/pasted for further communication with the ISMARA team in case of problems or for questions.

Further information on usage examples and result interpretation of ISMARA can be found on the ISMARA web site at:
https://ismara.unibas.ch/fcgi/mara

Copyright - SIB Swiss Institute of Bioinformatics"

                        MouseArea {
                            anchors.fill: parent
                            acceptedButtons: Qt.NoButton // we don't want to eat clicks on the Text
                            cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                        }

                        onLinkActivated: {
                            Qt.openUrlExternally(link)
                        }
                    }
                }
            }
        }
        Rectangle {
            height: 30
            //Layout.fillWidth: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10

            Button {
                text: "OK"
                anchors.centerIn: parent
                onClicked: {
                    userManual.hide();
                }
            }
        }
    }
}
