import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Window {
    id: versionDialog
    width: 400
    height: 200

    title: "Update info"

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        Text {
            id: versionMainText
            width: versionDialog.width

            // This setting makes the text wrap at word boundaries when it goes past the width of the Text object
            wrapMode: Text.WordWrap

            text: "<p><b>ISMARA client update is available</b><br>"+app.updateMessage+"</p>"

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton // we don't want to eat clicks on the Text
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }

            onLinkActivated: {
                Qt.openUrlExternally(link)
            }
        }

        Rectangle {
            height: 30
            //Layout.fillWidth: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10

            Button {
                text: "OK"
                anchors.centerIn: parent
                onClicked: {
                    versionDialog.hide();
                }
            }
        }
    }

}
