/**************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Installer Framework.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
**
** $QT_END_LICENSE$
**
**************************************************************************/

function Component()
{
    // constructor
    component.loaded.connect(this, Component.prototype.loaded);
    if (!installer.addWizardPage(component, "Page", QInstaller.TargetDirectory))
        console.log("Could not add the dynamic page.");
}

Component.prototype.isDefault = function()
{
    // select the component by default
    return true;
}

Component.prototype.createOperations = function() {
    try {
        // call the base create operations function
        component.createOperations();
        
    }
        
    catch (e) {
        print(e);
    }
    
    if (installer.value("os") === "win") {
        component.addOperation("CreateShortcut", "@TargetDir@/Ismara.exe", "@DesktopDir@/Ismara client.lnk");
    }

    if (installer.value("os") === "x11") {
	component.addOperation( "InstallIcons", "@TargetDir@/icons" );
        component.addOperation("CreateDesktopEntry", "@TargetDir@/ISMARA_client.desktop", 
        "Version=1.0\nType=Application\nTerminal=false\nExec=@TargetDir@/bin/start.ismara\nName=ISMARA_client\nIcon=@TargetDir@/logo/ismara.png\nName[en_US]=ISMARA_client");
        component.addOperation("Copy", "@TargetDir@/ISMARA_client.desktop", "@HomeDir@/Desktop/ISMARA_client.desktop");
    }


}

Component.prototype.loaded = function ()
{
    var page = gui.pageByObjectName("DynamicPage");
    if (page != null) {
        console.log("Connecting the dynamic page entered signal.");
        page.entered.connect(Component.prototype.dynamicPageEntered);
    }
}

Component.prototype.dynamicPageEntered = function ()
{
    var pageWidget = gui.pageWidgetByObjectName("DynamicPage");
    if (pageWidget != null) {
        console.log("Setting the widgets label text.")
        pageWidget.m_pageLabel.setText("The ISMARA client software allows you to preprocess big data files\non your own computer. Afterwards, only a small file needs to be uploaded \nto the ISMARA server for further analysis!");
	pageWidget.m_pageLabel.setWordWrap(true);
	//pageWidget.m_pageLabel.text = "ISMARA client allows you to preprocess big data files on your own computer.";
	//pageWidget.m_pageLabel2.text = "Afterwards a small file is uploaded to the ISMARA server for further analysis.";
    }
}
