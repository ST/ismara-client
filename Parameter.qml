import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

import "component-utils.js" as Utils

Rectangle {
    id: parameterRect
    objectName: "parameterRect"

    property alias processEnabled: processButton.enabled

    width: 650
    height: 50

    x: 30

    Rectangle {
        id: parameterColumnItem
        //color: "white"
        border.color: "#E6E6E6"
        color: "#F5F5F5"
        anchors.fill: parent
        anchors.margins: 10
        anchors.horizontalCenter: parent.horizontalCenter
        radius: 4
        height: 30

        Text {
            id: emailText
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.margins: 8
            anchors.leftMargin: 14
            text: "<b>Email:</b>"
        }

        TextField {
            id: emailInputText
            anchors.verticalCenter: emailText.verticalCenter
            anchors.left: emailText.right
            anchors.leftMargin: 4
            maximumLength: 250

            width: 225
            height: 22
            text: emailValue

            // control/validation: Only characters suitable for email addresses are allowed.
            //validator: RegExpValidator { regExp: /[A-Z\d.-]+@[A-Z\d-.]+\.[a-z]+/i }
            validator: RegExpValidator { regExp:/^$|^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i }

        }


        Text {
            id: projectText
            anchors.verticalCenter: emailText.verticalCenter
            anchors.left: emailInputText.right
            anchors.leftMargin: 20
            text: "<b>Project name:</b>"
        }

        TextField {
            id: projectInputText
            anchors.verticalCenter: emailText.verticalCenter
            anchors.left: projectText.right
            anchors.leftMargin: 4

            maximumLength: 30
            width: 225

            height: 22
            text: projectName
        }

    }

    Button {

        id: processButton

        anchors.verticalCenter: parameterColumnItem.verticalCenter
        anchors.left: parent.right
        anchors.leftMargin: 10
        anchors.margins: 10

        enabled: false
        state: "DISABLED"
        text: "Process data"

        style:  ButtonStyle {
                    background: Rectangle {
                        //color: "#D1F0FF"
                        //color: "#87C66D"
                        //color: "#4B2A91"
                        color: control.hovered ? '#6F55A7' : '#4B2A91'
                        opacity: 0.5
                        radius: 3
                        implicitWidth: 100
                        implicitHeight: 26
                        //border.color: "#A3A3A3"
                        border.color: "black"
                        border.width: processButton.enabled ? 1 : 2

                    }
                }

        onEnabledChanged: enabled ? processButton.state = "ENABLED" : processButton.state = "DISABLED"

        states: [
            State {
                name: "ENABLED"
                PropertyChanges { target: processButton; opacity: 1}
            },
            State {
                name: "DISABLED"
                PropertyChanges { target: processButton; opacity: 0.5}
            }
        ]

        transitions: [
            Transition {
                from: "DISABLED"
                to: "ENABLED"
                //ColorAnimation { target: button; duration: 100}

                NumberAnimation {
                    id: animateOpacityEnable
                    target: processButton
                    properties: "opacity"
                    from: 0.5
                    to: 1
                    duration: 900
                    easing {type: Easing.OutInBack; overshoot: 20}
               }


            },
            Transition {
                from: "ENABLED"
                to: "DISABLED"
                //ColorAnimation { target: button; duration: 100}
                NumberAnimation {
                    id: animateOpacityDisable
                    target: processButton
                    properties: "opacity"
                    from: 1
                    to: 0.5
                    duration: 1000
                    easing {type: Easing.InSine;}
               }
            }
        ]

        onClicked: {
            flag = new Object({filesareOK: true})
            process_start.start()
            if(flag.filesareOK == true) {
                emailValue = emailInputText.text
                projectName = projectInputText.text

                console.log("Calling process: "+progName)
                //console.log("Path: " + appPath)

                Utils.disableAll()
                app.jobid = progName + '-' + Date.now() // issue #41: Reset jobID
                Utils.addJob(jobid, projectName, "Running", "", "")
                // Reset the log information text area
                tabInfo.textArea.text = ""
                process.start()

            }
        }
    }

}

