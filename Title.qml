import QtQuick 2.4


// top of the page: Title + ISMARA help + Contact us
Rectangle {
    id: rectangleHeader

    x: 40
    y: 5

    Text {
        id: appTitle
        // THis text may have to be changed!
        text: app.programTitle
        font.bold: true
        font.pixelSize: 18
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Image {
        // id
        id: sibLogo

        // source
        source: 'img/sib_logo.png'

        // styling - aspect
        width: 104; height: 80
        fillMode: Image.PreserveAspectFit

        MouseArea {
            anchors.fill: parent
            onClicked: Qt.openUrlExternally("http://www.sib.swiss")
        }

    }
    Image {
        // id
        id: bzLogo

        // source
        source: 'img/BZ_logo_134x75.png'

        // positioning - anchoring
        anchors.left: sibLogo.right
        anchors.leftMargin: 5

        // styling - aspect
        width: 104; height: 80
        fillMode: Image.PreserveAspectFit

        MouseArea {
            anchors.fill: parent
            onClicked: Qt.openUrlExternally("http://www.biozentrum.unibas.ch")
        }
    }
} // end top of the page + ISMARA help + Contact us
