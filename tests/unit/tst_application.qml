import QtQuick.Controls 1.3
import QtTest 1.0

TestCase {

    ApplicationWindow {
        id: theTestApplication

        property string json_msg: ""
    }

    id: utest2

    function test_json_ok() {
        theTestApplication.json_msg = '{"type":"myType","severity":"mySeverity","msg":"myMessage"}';

        var patt = /^{"/;
        if(patt.test(theTestApplication.json_msg)) {
            var jsonObject = JSON.parse(theTestApplication.json_msg);

            var message = jsonObject.msg;
            compare(message, "myMessage");

            var severity = jsonObject.severity;
            compare(severity, "mySeverity");

            var type = jsonObject.type;
            compare(type, "myType");
        }
    }
}


