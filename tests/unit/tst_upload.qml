import QtQuick 2.4
import QtQuick.Controls 1.3
import QtTest 1.0
import QtProcess 0.1
import HttpUploader 0.1

TestCase {

    HttpUploader {
        id: theTestUploader
    }

    id: utest3

    function test_uploader_status_enum() {

        compare(HttpUploader.Aborting, 3);
        compare(HttpUploader.Done, 4);
        compare(HttpUploader.Loading, 2);
        compare(HttpUploader.Opened, 1);
        compare(HttpUploader.Unsent, 0);
    }

    function test_uploader()
    {
        theTestUploader.clear();
        theTestUploader.open("url_value");
        compare(theTestUploader.url, "url_value");
    }
}
