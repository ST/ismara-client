import QtQuick 2.4
import QtQuick.Controls 1.3
import QtTest 1.0
import QtProcess 0.1

TestCase {

    Process {
        id: theTestProcess
    }

    id: utest1

    function test_program_ok() {
        var program = 'rnaseq';
        theTestProcess.setProgram(program)
        compare(theTestProcess.program, "rnaseq")
    }

    function test_arguments_ok() {
        var args = ['genome','matrix','jobid','file1','file2'];
        theTestProcess.setArguments(args);
        compare(theTestProcess.arguments[0], "genome");
        compare(theTestProcess.arguments[1], "matrix");
        compare(theTestProcess.arguments[2], "jobid");
        compare(theTestProcess.arguments[3], "file1");
        compare(theTestProcess.arguments[4], "file2");
    }
}
