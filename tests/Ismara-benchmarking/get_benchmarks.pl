#!/usr/bin/env perl
use strict;
use warnings;
use feature 'say';
use Carp;
use Number::Bytes::Human qw(format_bytes parse_bytes);
#use Math::Round;
# cpanm install List::Compare
#use List::Compare;
#use File::Basename;
#use Spreadsheet::WriteExcel;

unless(@ARGV >= 1) {die "give at least one directory or several filenames\n"};

my @files = @ARGV;
my @header = ('Model','Memory (GB)','CPU (logical)','CPU (frequency Ghz','Array completion','Array total filesize','Array: sec/MB','Rnaseq completion','Rnaseq total filesize','RNAseq: sec/MB','Chipseq completion','Chipseq total filesize','Chipseq: sec/MB');
my @benchmark;

foreach (@files){
    my @array = get_data($_);
    push(@benchmark, \@array);
}
@benchmark = sort { lc($a->[0]) cmp lc($b->[0]) } @benchmark;
#@benchmark = sort {$a->[1] cmp $b->[1] || $a->[0] cmp $b->[0]} @benchmark;
unshift(@benchmark, \@header);
for (my $i=0;$i < scalar @benchmark;$i++){
    for (my $j=0; $j < scalar(@{$benchmark[$i]});$j++){
       print "$benchmark[$i]->[$j]\t";
    }
    say "";
}

exit;

sub get_data{
    my ($filename)= @_;
    open(my $fh,"<", $filename) or die "Could not open file: '$filename' $!";
    my @array_list;
    while ( my $line = <$fh>) {
        chomp $line;
        if ($line =~ /^Model.*model: (.*)$/){ push @array_list,$1; }
        elsif ($line =~ /^Memory.*memsize: (.*)$/){ push @array_list,$1/(1024*1024*1024); }
        elsif ($line =~ /^Logical.*logicalcpu: (.*)$/){ push @array_list,$1; }
        elsif ($line =~ /^CPU.*cpufrequency: (.*)$/){ push @array_list,$1/1000000000; }
        elsif ($line =~ /^microarray\t(.*)\t(.*)$/){ my $hour=($1/(60*60))%24;my $min=($1/60)%60;my $sec=($1%60);my $time=$hour.'h'.$min."m".$sec."s"; my $panuStats = $1/($2 / (1024 * 1024)); push @array_list,$time,format_bytes($2, bs => 1000), format_bytes($panuStats, bs => 1024, round_style => 'trunc', precision => 3);}
        elsif ($line =~ /^rnaseq\t(.*)\t(.*)$/){ my $hour=($1/(60*60))%24;my $min=($1/60)%60;my $sec=($1%60);my $time=$hour.'h'.$min."m".$sec."s"; my $panuStats = $1/($2 / (1024 * 1024)); push @array_list,$time,format_bytes($2, bs => 1000), format_bytes($panuStats, bs => 1024, round_style => 'trunc', precision => 3);}        elsif ($line =~ /^chipseq\t(.*)\t(.*)$/){ my $hour=($1/(60*60))%24;my $min=($1/60)%60;my $sec=($1%60);my $time=$hour.'h'.$min."m".$sec."s"; my $panuStats = $1/($2 / (1024 * 1024)); push @array_list,$time,format_bytes($2, bs => 1000), format_bytes($panuStats, bs => 1024, round_style => 'trunc', precision => 3);
        }
        else {;}
    }
    return @array_list;
}
