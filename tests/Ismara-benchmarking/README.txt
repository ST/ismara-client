Benchmarking suite for the Ismara Mac OS X standalone application.

REQUIREMENTS:
- R
The mac running the benchmark should have R version 3.2.0 minimum.
You can check that by executing the following command on the terminal:
Rscript --version

(more info: http://cran.r-project.org/doc/manuals/r-release/R-admin.html#Uninstalling-under-OS-X
# to install a clean new version of R, uninstall any previous version:
rm -rf /Library/Frameworks/R.framework /Applications/R.app /usr/bin/R /usr/bin/Rscript
# the 3.2.0 version can be downloaded from:
http://stat.ethz.ch/CRAN/bin/macosx/R-3.2.0.pkg)

- Copy the Ismara.app application bundle into the Ismara-benchmarking directory.

- Put your own input files for the different type in the respective directories:
Ismara-mac-benchmarking/benchmark/
- microarrays/
- rnaseq/
- chipseq/

INSTRUCTIONS:
1) execute the script 'benchmark_1.sh'
# this will copy the necessary files into the benchmark directory, including data, utils, scripts, etc.

2) execute the script 'benchmark.sh'
# it is not mandatory to modify the default parameters of the script 'benchmark_config.sh' 

REMARKS:
Once the script 'benchmark.sh' is finished, the following data will be written:

- benchmark summary: 2015-06-11_09h31m19s-JOBID.txt
you can extract a summary table by executing from one or several files:
perl get_benchmarks.pl 2015-06-11_10h04m35s-1434009875.txt
or
perl get_benchmarks.pl 2015-06-* 

logs:
- array-JOBID.log
- rnaseq-JOBID.log
- chipseq-JOBID.log

tmp:
- array-JOBID/
- rnaseq-JOBID/
- chipseq-JOBID/

data/output:
- results-array-JOBID.tar.gz
- results-rnaseq-JOBID.tar.gz
- results-chipseq-JOBID.tar.gz

logs, tmp and data/output directories are not cleaned automatically.
