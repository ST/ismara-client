#!/bin/bash

USER=$(whoami)
export ROOT=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cp -r $ROOT/Ismara.app/Contents/MacOS/data .
cp -r $ROOT/Ismara.app/Contents/MacOS/scripts .
cp -r $ROOT/Ismara.app/Contents/MacOS/utils .
mkdir $ROOT/logs

# clean up data/ouput if necessary :-)

ls -l

exit;