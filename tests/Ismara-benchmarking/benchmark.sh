#!/bin/bash

export ROOT=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export REPORTID=$(date '+%Y-%m-%d_%Hh%Mm%Ss-%s')
source $ROOT/benchmark_config.sh

### usage: modify benchmark_config.sh to provide paths to input files then:
### ./benchmark.sh

echo $REPORT

echo "Model: $SPECMODEL" | tee -a $REPORT
echo "OS: $OS" | tee -a $REPORT
echo "Memory Size: $SPECMEMSIZE" | tee -a $REPORT
echo "Physical number of cpu: $SPECPHYCPU" | tee -a $REPORT
echo "Logical number of cpu: $SPECLOGCPU" | tee -a $REPORT
echo "CPU frequency: $SPECFREQCPU" | tee -a $REPORT
echo "Python version: $PYTHON_VERSION" | tee -a $REPORT
echo "Python lib path: $PYTHONPATH" | tee -a $REPORT
echo "R version: $R_VERSION" | tee -a $REPORT
echo "R lib path: $R_LIBS" | tee -a $REPORT
echo "" | tee -a $REPORT
echo "microarray" | tee -a $REPORT

##############
# microarray #
##############
genome=hg19
JOBID=array-$(date +%s)
micro=SwissReg_TargetScan
directory=$MISD
directory="${directory%"${directory##*[![:space:]]}"}"   # remove trailing whitespace characters

inputarray=$(ls $MISD/*)
filesizemicro=
for i in ${inputarray[@]}
do
if [ $OS == "osx" ]; then
x=$(stat -f '%z' $i)
echo $i
(( filesizemicro += x ))
else
x=$(stat --format=%s $i)
echo $i
(( filesizemicro += x ))
fi
done
starttime=$(date +'%s')
echo "microarray started at $starttime,  $(date '+%Y-%m-%d_%Hh%Mm%Ss')" | tee -a $REPORT
echo "genome: $genome" | tee -a $REPORT
echo "JOBID: $JOBID" | tee -a $REPORT
echo "Micro: $micro" | tee -a $REPORT
inputarray=$(ls $MISD/*)
printf '%s\n' "${inputarray[@]}" |tee -a $REPORT

# Hard-coded paths!
source $ROOT/scripts/env.sh "microarray.sh"
source $ROOT/scripts/common_utils.sh

json_out "info" 1 "root is set to $ROOT" true
json_out "info" 1 "Printing log to $LOG" true
json_out "info" 1 "Genome parameter: $genome" true
json_out "info" 1 "Matrix parameter: $micro" true
json_out "info" 1 "Directory parameter: $directory" true

mkdir -p $OUTPUT
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't create directory $OUTPUT" true 1
[[ ! -w "$OUTPUT" ]] && json_out "error" 1 "Can't write to directory $OUTPUT" true 1

mkdir -p $TMP
[[ ! -d "$TMP" ]] && json_out "error" 1 "Can't create directory $TMP" true 1
[[ ! -w "$TMP" ]] && json_out "error" 1 "Can't write to directory $TMP" true 1

json_out "info" 1 "==> Running Rscript process_affy_files.R $directory $TMP `date`" true
Rscript $IPATH/process_affy_files.R "$directory" $TMP || json_out "error" 1 "Problem with process_affy_files.R" true 1

json_out "info" 1 "R script terminated `date`" true

json_out "info" 1 "==> Running summarizeExpression `date`" true
prepare_output "microarray"

json_out "info" 1 "==> END workflow: `date`" true
json_out "info" 1 "Final result is available in $OUTPUT/results-$JOBID.tar.gz ... to be transferred to ISMARA" true

echo "microarray ended at $(date +'%s')" | tee -a $REPORT
completionmicro=$(($(date +'%s') - $starttime))
echo "Completed in $completionmicro seconds" | tee -a $REPORT
echo "" | tee -a $REPORT
echo
echo "rnaseq"
echo
##############
#   rnaseq   #
##############
genome=hg19
JOBID=rnaseq-$(date +%s)
micro=SwissReg_TargetScan
directory=
directory="${directory%"${directory##*[![:space:]]}"}"   # remove trailing whitespace characters

starttime=$(date +'%s')

# Hard-coded paths!
source $ROOT/scripts/env.sh "rnaseq.sh"
source $ROOT/scripts/common_utils.sh

array=($RISF/*.bed.gz)
len=${#array[@]}
echo
printf '%s\n' "${array[@]}"
echo

json_out "info" 1 "root is set to $ROOT" false
json_out "info" 1 "Printing log to $LOG" false
json_out "info" 1 "Number of arguments: $len" false


mkdir -p $OUTPUT
#[[ ! -d "$OUTPUT" ]] && exit_error "Can't create directory "$OUTPUT
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't create directory "$OUTPUT true 1
#errortype, severity, message, log-true/false exit_code (exit if set)
#[[ ! -w "$OUTPUT" ]] && exit_error "Can't write to directory "$OUTPUT
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't write to directory "$OUTPUT true 1

mkdir -p $TMP
#[[ ! -d "$TMP" ]] && exit_error "Can't create directory "$TMP
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't create directory "$TMP true 1
#[[ ! -w "$TMP" ]] && exit_error "Can't write to directory "$TMP
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't write to directory "$TMP true 1

mkdir -p $TMP/processing
#[[ ! -d "$TMP/processing" ]] && exit_error "Can't create directory "$TMP/processing
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't create directory "$TMP/processing true 1
#[[ ! -w "$TMP/processing" ]] && exit_error "Can't write to directory "$TMP/processing
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't write to directory "$TMP/processing true 1

# Severine
# The script won't work without removing the latest expression dir!
rm -rf $TMP/expression

mkdir -p $TMP/expression
#[[ ! -d "$TMP/expression" ]] && exit_error "Can't create directory "$TMP/expression
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't create directory "$TMP/expression true 1
#[[ ! -w "$TMP/expression" ]] && exit_error "Can't write to directory "$TMP/expression
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't write to directory "$TMP/expression true 1

cd $IPATH
#echo "==> START workflow: " `date` >$LOG
json_out "info" 1 "Start workflow" true

# Foreach file
for (( i = 0 ; i < ${#array[@]} ; i++ ))
do
export FILEPATH=${array[$i]}
export FILE=$(basename $FILEPATH)

#echo "Formatting and creating index for file: $FILE" `date` >>$LOG
json_out "info" 1 "Formatting and creating index for file: $FILE" true

format-bed-and-do-tbi-index $FILEPATH $FILE
#echo "End formatting and indexing " `date`>>$LOG
json_out "info" 1 "End formatting and creating index file: $FILE" true

#echo "Running readRegion.py: " `date`>>$LOG
json_out "info" 1 "Running readRegion.py" true

json_out "progress" 1 1 false

readRegion_Cmd1="$IPATH/readRegion.py -t $INPUT/$genome/refSeqAli $INPUT/$genome/all_mrna -p $INPUT/$genome/clusters_150_gene_associations -i $TMP/processing/$FILE --expr-dir $TMP/expression"
python $readRegion_Cmd1 || json_out "error" 1 "Script $0 Line $LINENO: Problem with:	python $readRegion_Cmd1 Error: $( { python $readRegion_Cmd1; } )" true $?

json_out "info" 1 "End readRegion" true

done

mergeReads_Cmd="$IPATH/merge_reads.py -d $TMP/expression -e $TMP/expression/expression.tab"

json_out "info" 1 "Running: python $mergeReads_Cmd" true

json_out "progress" 1 1 false

python $mergeReads_Cmd || json_out "error" 1 "Script $0 Line $LINENO: Problem with: python $mergeReads_Cmd Error: $( { python $mergeReads_Cmd; } )" true $?

prepare_output "rnaseq"

json_out "info" 1 "End workflow" true
json_out "info" 1 "Final result is available in $OUTPUT/results-$JOBID.tar.gz ... to be transferred to ISMARA" false

echo "rnaseq started at $starttime,  $(date '+%Y-%m-%d_%Hh%Mm%Ss')" | tee -a $REPORT
echo "genome: $genome" | tee -a $REPORT
echo "JOBID: $JOBID" | tee -a $REPORT
echo "Micro: $micro" | tee -a $REPORT
inputarray=$(ls $RISF/*)
filesizernaseq=
for i in ${inputarray[@]}
do
if [ $OS == "osx" ]; then
x=$(stat -f '%z' $i)
echo $i
(( filesizemicro += x ))
else
x=$(stat --format=%s $i)
echo $i
(( filesizemicro += x ))
fi
done
printf '%s\n' "${inputarray[@]}" |tee -a $REPORT
echo "rnaseq ended at $(date +'%s')" | tee -a $REPORT
completionrnaseq=$(($(date +'%s') - $starttime))
echo "Completed in $completionrnaseq seconds" | tee -a $REPORT
echo "" | tee -a $REPORT
echo
echo "chipseq"
echo

##############
#   chipseq  #
##############
genome=hg19
JOBID=chipseq-$(date +%s)
micro=SwissReg
directory=
directory="${directory%"${directory##*[![:space:]]}"}"   # remove trailing whitespace characters

starttime=$(date +'%s')

# Hard-coded paths!
source $ROOT/scripts/env.sh "chipseq.sh"
source $ROOT/scripts/common_utils.sh

array=($CISF/*.bed.gz)
len=${#array[@]}
echo
printf '%s\n' "${array[@]}"
echo

json_out "info" 1 "root is set to $ROOT" false
json_out "info" 1 "Printing log to $LOG" false
json_out "info" 1 "Genome parameter: $genome" true
json_out "info" 1 "Matrix parameter: $micro" true
json_out "info" 1 "JOBID parameter: $JOBID" true
json_out "info" 1 "Number of arguments: $len" false

mkdir -p $OUTPUT
[[ ! -d "$OUTPUT" ]] && json_out "error" 1 "Can't create directory $OUTPUT" true 1
[[ ! -w "$OUTPUT" ]] && json_out "error" 1 "Can't write to directory $OUTPUT" true 1

mkdir -p $TMP
[[ ! -d "$TMP" ]] && json_out "error" 1 "Can't create directory $TMP" true 1
[[ ! -w "$TMP" ]] && json_out "error" 1 "Can't write to directory $TMP" true 1

mkdir -p $TMP/processing
[[ ! -d "$TMP/processing" ]] && json_out "error" 1 "Can't create directory $TMP/processing" true 1
[[ ! -w "$TMP/processing" ]] && json_out "error" 1 "Can't write to directory $TMP/processing" true 1

# Severine
# The script won't work without removing the latest expression dir!
rm -rf $TMP/expression

mkdir -p $TMP/expression
[[ ! -d "$TMP/expression" ]] && json_out "error" 1 "Can't create directory $TMP/expression" true 1
[[ ! -w "$TMP/expression" ]] && json_out "error" 1 "Can't write to directory $TMP/expression" true 1

cd $IPATH
json_out "info" 1 "==> START workflow: `date`" true

# Foreach file
for (( i = 0 ; i < ${#array[@]} ; i++ ))
do
export FILEPATH=${array[$i]}
export FILE=$(basename $FILEPATH)

json_out "info" 1 "Formatting and creating index for file: $FILE `date`" true
format-bed-and-do-tbi-index $FILEPATH $FILE
json_out "info" 1 "End formatting and indexing `date`" true

json_out "info" 1 "Running chipseq_readRegion.py: `date`" true
chipseq_readRegion_Cmd="$IPATH/chipseq_readRegion.py -p $INPUT/$genome/clusters_150_gene_associations -i $TMP/processing/$FILE --expr-dir $TMP/expression"
python $chipseq_readRegion_Cmd || json_out "error" 1 "Error with $chipseq_readRegion_Cmd" true 1
json_out "info" 1 "End chipseq_readRegion: `date`" true

done

json_out "info" 1 "Starting merge_reads_chipseq: `date`" true
merge_reads_chipseq_Cmd="$IPATH/merge_reads_chipseq.py -o $genome -d $TMP/expression -e $TMP/expression/expression.tab"
python $merge_reads_chipseq_Cmd || json_out "error" 1 "Error with $merge_reads_chipseq_Cmd: $?" true 1
json_out "info" 1 "End merge_reads_chipseq: `date`" true

json_out "info" 1 "Starting: quantile_normalization.R: `date`" true
Rscript $IPATH/quantile_normalization.R $TMP/expression || json_out "error" 1 "Error with Rscript $IPATH/quantile_normalization.R $TMP/expression : $?" true 1
json_out "info" 1 "End quantile_normalization.R: `date`" true

prepare_output "chipseq"

json_out "info" 1 "==> END workflow: `date`" true
json_out "info" 1 "Final result is available in $OUTPUT/results-$JOBID.tar.gz ... to be transferred to ISMARA" true


echo "chipseq started at $starttime,  $(date '+%Y-%m-%d_%Hh%Mm%Ss')" | tee -a $REPORT
echo "genome: $genome" | tee -a $REPORT
echo "JOBID: $JOBID" | tee -a $REPORT
echo "Micro: $micro" | tee -a $REPORT
inputarray=$(ls $CISF/*)
filesizechipseq=
for i in ${inputarray[@]}
do
if [ $OS == "osx" ]; then
x=$(stat -f '%z' $i)
echo $i
(( filesizemicro += x ))
else
x=$(stat --format=%s $i)
echo $i
(( filesizemicro += x ))
fi
done
printf '%s\n' "${inputarray[@]}" |tee -a $REPORT
echo "inputarray: $inputarray" | tee -a $REPORT
echo "chipseq ended at $(date +'%s')" | tee -a $REPORT
completionchipseq=$(($(date +'%s') - $starttime))
echo "Completed in $completionchipseq seconds" | tee -a $REPORT

echo "" | tee -a $REPORT

# clean data/output directory ? :-)

echo "" | tee -a $REPORT
echo -e "microarray\t$completionmicro\t$filesizemicro" | tee -a $REPORT
echo -e "rnaseq\t$completionrnaseq\t$filesizernaseq" | tee -a $REPORT
echo -e "chipseq\t$completionchipseq\t$filesizechipseq" | tee -a $REPORT
echo ""
echo "DONE"
echo ""
echo "If you want to clean the log, data/output and tmp directories, execute the following command:"
echo "rm -rf data/output/* logs/* tmp/*"
echo ""



