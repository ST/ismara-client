#!/bin/bash

# Input sequences
# enter path to Microarray Input Sequence Directory
export MISD=$ROOT/benchmark/microarray

# enter path to Rnaseq Input Sequences Files
export RISF=$ROOT/benchmark/rnaseq

# enter path to chipseq sequences
export CISF=$ROOT/benchmark/chipseq

# path to the report file
export REPORT=$ROOT/$REPORTID.txt

################################################
# you should not need to change anything below #
################################################

# Set env variables for ISMARA benchmarking
# ROOT is set in the upstream script

# Find out which system we are running on:
if [ "$(uname)" == "Darwin" ]; then
    export OS="osx"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    export OS="linux"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    export OS="win"
fi

# checking python version, should be 2.7.6 for mac and 2.7.9 for linux
PYTHON_VERSION=$(python -V 2>&1)
export PYTHON_VERSION

# Python libraries
if [ $OS == "osx" ]; then
export PYTHONPATH=$ROOT/scripts/python_libs/$OS
export SPECMEMSIZE=$(sysctl hw.memsize)
export SPECPHYCPU=$(sysctl hw.physicalcpu)
export SPECLOGCPU=$(sysctl hw.logicalcpu)
export SPECFREQCPU=$(sysctl hw.cpufrequency)
export SPECMODEL=$(sysctl hw.model)
else
export PYTHONPATH=$PYTHONPATH:$ROOT/scripts/python_libs/$OS/Library/Python/2.7/dist-packages:$ROOT/scripts/python_libs/$OS/Library/Python/2.7/site-packages:$ROOT/scripts/python_libs/$OS/usr
export SPECMEMSIZE=$(cat /proc/meminfo | grep MemTotal)
export SPECPHYCPU=$(lscpu |grep -e "^Core(s) per socket" -e "^Socket(s)")
export SPECLOGCPU=$(lscpu |grep -e "^CPU(s)")
export SPECFREQCPU=$(lscpu |grep -e "CPU MHz")
export SPECMODEL=$(lscpu |grep -e "Vendor ID" -e "Architecture" -e "CPU family")
fi


# checking R Version, should be 3.2 for mac and 3.1.2 for linux

#TODO CHECK VERSION NB
R_VERSION=$(Rscript --version 2>&1)
export R_VERSION

# R libraries
export R_LIBS=$ROOT/scripts/R_libs/$OS

# Contains required utilities such as bed-sort, bgzip, ...
# all the binaries were copied to $UTILS
export UTILS=$ROOT/utils/$OS
export PATH=$PATH:$UTILS

# SET LOG
export LOG=$ROOT/scripts/$JOBID.log

# Location of 4 input files:
# all_mrna  clusters_150_gene_associations  refSeqAli  sitecount_matrix_allProms
# Same questions as above for the libraries
export INPUT=$ROOT/data/src

# Location of the files requested for the ISMARA web service
# (Downstream job)
export OUTPUT=$ROOT/data/output

# Location of processing scripts from Mikhail
export IPATH=$ROOT/scripts

# Location of temporary files
export TMP=$ROOT/tmp/$JOBID
