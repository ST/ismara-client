import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtTest 1.0
import "../.."

Main {
    id: ismaraApplication
    objectName: "ismaraApplication"

    width: 30
    height: 40

    // in "normal" run, appPath is defined by main.cpp
    // in "test" mode, we have to redefine the variable
    // to avoid warnings in the qmltestrunner's output.
    property string appPath: ""

    TestCase {
        id: fileChooserTestCase
        name: "fileChooserTestCase"

        function test_filechooser () {
            var filechooser = findChild(ismaraApplication, "fileChooser");
            if (filechooser !== null)
            {
                var model = filechooser.model
                if (model !== null)
                {
                    // model already contains one ListElement
                    compare(model.count, 1, "Initially, one row in file chooser, count should be 1!")

                    model.insert(1, {"deleteFlag": true, "files": "/my/test/file"});
                    compare(model.count, 2, "Inserted one row in file chooser, count should be 2!")

                    model.remove(1);
                    compare(model.count, 1, "Removed one row in file chooser, count should be 1!")

                    // insert 3 element and clear using the button
                    // there should remain only one row in the file chooser
                    model.insert(1, {"deleteFlag": true, "files": "/my/test/file"});
                    model.insert(2, {"deleteFlag": true, "files": "/my/test/file"});
                    model.insert(3, {"deleteFlag": true, "files": "/my/test/file"});
                    compare(model.count, 4, "Inserted 3 rows in file chooser, count should be 4!")

                    var clearBtn = findChild(ismaraApplication, "clearButton");
                    if (clearBtn !== null)
                    {
                        // position of the button is x=0 ; y=10
                        mouseClick(clearBtn, 0, 10);
                        compare(model.count, 1, "Cleared the file chooser, count should be 1!")
                    }
                    else
                    {
                        fail("Clear button object null!")
                    }
                }
                else
                {
                    fail("File chooser model object null!")
                }
            }
            else
            {
                fail("fileChooserTest > test_filechooser: File chooser model null!")
            }
        }
    }
}
