import QtQuick 2.4
import QtQuick.Controls 1.3
import QtTest 1.0
import "../.."

// Problem with ssl on MacOS X
// qmltestrunner outputs error messages
// which popups a confirmation box when building/running
// the application in DEBUG mode.
// We have to find a way to ignore those messages in Qt
// or update openSSL on MacOS X:
// $ openssl version
// OpenSSL 0.9.8zd 8 Jan 2015
// minimum required by Qt 5.4: OpenSSL is 1.0.0 or later.

// I did not manage to make qmltestrunner silent, nor to
// change the qmake options.
// I can try to trick QtCreator in order to remember my choice yes I press
// "Ignore errors messages".

Main {
    id: ismaraApplication
    objectName: "ismaraApplication"
    width: 30
    height: 40

    // in "normal" run, appPath is defined by main.cpp
    // in "test" mode, we have to redefine the variable
    // to avoid warnings in the qmltestrunner's output.
    property string appPath: ""

    TestCase {
        id: ismaraApplicationTest
        name: "ismaraApplicationTest"

        function test_chooseoption () {

            // chooseoption component of main
            var chooseoption_obj = findChild(ismaraApplication, "chooseOptions");
            if (chooseoption_obj !== null)
            {
                // Panel should be enabled
                compare(chooseoption_obj.enabled, true, "Initial state: chooseOptionPanel shoudl be enabled!");

                // data type panel
                var rnaseq_obj = findChild(chooseoption_obj, "rnaseq");
                var chipseq_obj = findChild(chooseoption_obj, "chipseq");
                var microarray_obj = findChild(chooseoption_obj, "microarray");

                // miRNA checkbox
                var mirna_obj = findChild(chooseoption_obj, "microRNACheckbox");

                // Genome panel
                var genome_panel_obj = findChild(chooseoption_obj, "genomeGroup")

                if (rnaseq_obj !== null && chipseq_obj !== null && microarray_obj !== null && mirna_obj !== null && genome_panel_obj !== null)
                {
                    /************************************************************************************************
                      initial state: microarray_obj is checked, rnaseq_obj is not checked, chipseq_obj is not checked
                                     mirna_obj is checked
                                     genome_panel_obj is not visible
                    ************************************************************************************************/
                    // data types
                    compare(rnaseq_obj.checked, false,    "Initial state: RNASeq should not be checked!");
                    compare(chipseq_obj.checked, false,   "Initial state: ChipSeq should not be checked!");
                    compare(microarray_obj.checked, true, "Initial state: Microarray should be checked!");
                    // Program name
                    compare(progName, "microarray", "Initial state: Program name should be 'microarray'!");
                    // miRNA
                    compare(mirna_obj.checked, true, "Initial state: miRNA should be checked!");


                    /************************************************************************************************
                      Use case #1: Event=click on rnaseq_obj:
                                   microarray_obj is not checked, rnaseq_obj is checked, chipseq_obj is not checked
                                   mirna_obj is checked
                                   genome_panel_obj is visible
                    ************************************************************************************************/
                    mouseClick(rnaseq_obj, 0, 7);
                    // data types
                    compare(rnaseq_obj.checked, true, "Use case #1: RNASeq should be checked!");
                    compare(chipseq_obj.checked, false, "Use case #1: ChipSeq should not be checked!");
                    compare(microarray_obj.checked, false, "Use case #1: Microarray should not be checked!");
                    // Program name
                    compare(progName, "rnaseq", "Use case #1: Program name should be 'rnaseq'!");
                    // miRNA
                    compare(mirna_obj.checked, true, "Use case #1: miRNA should be checked!");


                    /************************************************************************************************
                      Use case #2: Event=click on chipseq_obj:
                                   microarray_obj is not checked, rnaseq_obj is not checked, chipseq_obj is checked
                                   mirna_obj is not checked
                                   genome_panel_obj is visible
                    ************************************************************************************************/
                    mouseClick(chipseq_obj, 0, 8)
                    // data types
                    compare(rnaseq_obj.checked, false, "Use case #2: RNASeq should not be checked!");
                    compare(chipseq_obj.checked, true, "Use case #2: ChipSeq should be checked!");
                    compare(microarray_obj.checked, false, "Use case #2: Microarray should not be checked!");
                    // Program name
                    compare(progName, "chipseq", "Use case #2: Program name should be 'chipseq'!");
                    // miRNA
                    compare(mirna_obj.checked, false, "Use case #2: miRNA should not be checked!");


                    /************************************************************************************************
                      Use case #2: Event=click on chipseq_obj:
                                   microarray_obj is not checked, rnaseq_obj is not checked, chipseq_obj is checked
                                   mirna_obj is not checked
                                   genome_panel_obj is visible
                    ************************************************************************************************/
                    mouseClick(microarray_obj, 0, 10)
                    // data types
                    compare(rnaseq_obj.checked, false,    "Use case #3: RNASeq should not be checked!");
                    compare(chipseq_obj.checked, false,   "Use case #3: ChipSeq should not be checked!");
                    compare(microarray_obj.checked, true, "Use case #3: Microarray should be checked!");
                    // Program name
                    compare(progName, "microarray", "Use case #3: Program name should be 'microarray'!");
                    // miRNA
                    compare(mirna_obj.checked, true, "Use case #3: miRNA should be checked!");

                }
                else {
                    fail("Integration tests: tst_chooseoption - test_chooseoption function: one object null!")
                }
            }
            else {
                fail("Integration tests: tst_chooseoption - test_chooseoption function: ChooseOption null!")
            }
        }
    }
}
