import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Window {
    id: nointernetDialog
    width: 350
    height: 170

    title: "Warning: No Internet connection"

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        Text {
            id: nointernetMainText
            width: nointernetDialog.width

            // This setting makes the text wrap at word boundaries when it goes past the width of the Text object
            wrapMode: Text.WordWrap

            text: "<p><b>There was a problem connecting to the Internet</b></p><p>Ismara client requires internet connection to send <br>processed results to Ismara server.</p>"

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton // we don't want to eat clicks on the Text
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }

            onLinkActivated: {
                Qt.openUrlExternally(link)
            }
        }

        Rectangle {
            height: 30
            //Layout.fillWidth: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10

            Button {
                text: "OK"
                anchors.centerIn: parent
                onClicked: {
                    nointernetDialog.hide();
                }
            }
        }
    }

}
