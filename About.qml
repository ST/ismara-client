import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Window {
    id: aboutDialog
    width: 820
    height: 700

    title: "About "+app.programTitle

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        Text {
            id: aboutDialogMainText
            width: aboutDialog.width

            // This setting makes the text wrap at word boundaries when it goes past the width of the Text object
            wrapMode: Text.WordWrap

            text: "<p><b>ISMARA client</b> v" + version + "</p>
<p>created at the <b><a href='http://www.sib.swiss'>SIB Swiss Institute of Bioinformatics</a></b> by the groups:</p>
<ul>
<li><a href='http://www.sib.swiss/van-nimwegen-erik'>Genome Systems Biology</a></b>: M. Pachkov and E. van Nimwegen
<li><a href='http://www.sib.swiss/services-infrastructure/technology-at-sib'>SIB Technology</a></b>: P. Artimo, S. Duvaud, V. Ioannidis and H. Stockinger
</ul>

<p>Copyright - SIB Swiss Institute of Bioinformatics<br/>
License: <a href='https://www.gnu.org/licenses/gpl-2.0.txt'>GPL 2</a></p>

<p>ISMARA client uses the following software:</p>

<ul>
<li>Main R packages and their dependencies
<ul>
<li><a href='http://cran.r-project.org/web/packages/gtools/index.html'>gtools</a></li>
<li><a href='http://www.bioconductor.org/packages/release/bioc/html/affy.html'>affy</a></li>
<li><a href='http://www.bioconductor.org/packages/release/bioc/html/gcrma.html'>gcrma</a></li>
<li><a href='http://www.bioconductor.org/packages/release/bioc/html/preprocessCore.html'>preprocessCore</a></li>
<li><a href='http://cran.r-project.org/web/packages/mclust/index.html'>mclust</a></li>
</ul>
</li>
<li>Main Python libraries and their dependencies
<ul>
<li><a href='https://pypi.python.org/pypi/bx-python/0.7.2'>bx-python</a> Copyright 2005-2015 The Pennsylvania State University, Copyright 2013-2015 The Johns Hopkins University</li>
<li><a href='https://pypi.python.org/pypi/pysam/0.8.3'>pysam</a> Copyright 2008-2009 Genome Research Ltd.</li>
<li><a href='http://www.scipy.org/scipylib/index.html'>scipy</a> Copyright 2003-2013, SciPy Developers.</li>
<li><a href='http://www.numpy.org'>numpy</a> Copyright 2005-2013, NumPy Developers.</li>
</ul>
</li>
<li>External packages:
  <ul>
   <li><a href='https://github.com/samtools/htslib/archive/1.2.zip'>samtools</a> and <a href='https://github.com/samtools/htslib/archive/1.2.zip'>htslib</a> v1.2 (bgzip, samtools, tabix)</li>
   <li><a href='http://bedops.readthedocs.org/en/latest/'>bedops</a> v4.2.14 (sort-bed)</li>
  </ul>
</li>
</ul>
<p>ISMARA client was developed using the <a href='http://www.qt.io'>Qt cross-Platform application framework</a>.</p>

<p><b>Version history</b>
<ul>
<li>v1.1.2: added 'clear' button for all jobs</li>
<li>v1.1.1: extended set of motifs (genome version hg19 (updated) and mm10 (new))</li>
<li>v1.1.0: speed improvement (introduced multi-core usage to process several files in parallel)</li>
</ul>
</p>
"

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton // we don't want to eat clicks on the Text
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }

            onLinkActivated: {
                Qt.openUrlExternally(link)
            }
        }

        Rectangle {
            height: 30
            //Layout.fillWidth: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10

            Button {
                text: "OK"
                anchors.centerIn: parent
                onClicked: {
                    aboutDialog.hide();
                }
            }
        }
    }

}
