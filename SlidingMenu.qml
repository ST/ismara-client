import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import "component-utils.js" as Utils


Rectangle {
    id: containerRect

    width: 330
    //height: 230
    height: 180

    Rectangle {
        id: gv_
        //anchors.centerIn: parent
        // 160 when not shown fully, 280 when fully shown
        // width: 160
        width: 280
        height: 140
        border.color: "lightgrey"
        radius: 5
        x: 40
        //y: 40
        property bool menu_shown: true

        Component.onCompleted:{
            Utils.update_data_type(progName);
        }


        // do we want to expand the menu, comment if not
        //Behavior on width { PropertyAnimation { target: gv_; properties: "width"; duration: 300; easing.type: Easing.InOutBack } }

        /* this rectangle contains the "menu" */
        Rectangle {

            id: menu_view_

            anchors.top: parent.top

            width: 135

            anchors.left: parent.left

            transform: Translate {
                id: slide_menu
                //x: 100
                x: 140
                Behavior on x { NumberAnimation { duration: 400; easing.type: Easing.OutQuad } }
            }

            /* menu content */
            ListView {
                id: genomeGroupList
                opacity: gv_.menu_shown ? 1 : 0.3
                Behavior on opacity { NumberAnimation { duration: 400 } }


                enabled: gv_.menu_shown ? 1 : 0
                anchors { fill: parent; margins: 10 }

                GroupBox
                {
                    // id
                    id: genomeGroup
                    objectName: "genomeGroup"

                    width: 115
                    title: "Genome versions: "

                    ColumnLayout {

                        ExclusiveGroup {
                            id: genome
                        }

                        RadioButton {
                            id: hg19
                            text: "hg19"

                            // tooltip: "Human genome version 19"
                            exclusiveGroup: genome
                            onClicked:{
                                genomeName = "hg19"
                                Utils.reenable_file_selection()
                            }


                        }

                        RadioButton {
                            id: hg18
                            text: "hg18"
                            exclusiveGroup: genome
                            onClicked:{
                                genomeName = "hg18"
                                Utils.reenable_file_selection()
                            }
                        }

                        RadioButton {
                            id: mm10
                            text: "mm10"
                            exclusiveGroup: genome
                            onClicked:{
                                genomeName = "mm10"
                                Utils.reenable_file_selection()
                            }
                        }

                        RadioButton {
                            id: mm9
                            text: "mm9"
                            exclusiveGroup: genome
                            onClicked:{
                                genomeName = "mm9"
                                Utils.reenable_file_selection()
                            }
                        }

                    }
                }
            }
        }

        /* this rectangle contains the "normal" view in your app */

        /* this is the menu shadow */
        BorderImage {
            id: menu_shadow_

            anchors.fill: normal_view_
            anchors { leftMargin: -1; topMargin: -4; rightMargin: -8; bottomMargin: -4 }
            border { left: 10; top: 10; right: 10; bottom: 10 }
            source: "img/shadow.png"
        }

        Rectangle {
            id: normal_view_
            //anchors.fill: parent
            anchors { top: parent.top; bottom: parent.bottom; left: parent.left;}
            width: 130

            color: "white"
            border.color: "lightgrey"

            /* "normal" content */
            ListView {
                width: 130

                anchors { top: parent.top; bottom: parent.bottom; left: parent.left; margins: 10 }
                clip: true;

                GroupBox
                {
                    title: "Data type:   "
                    width: 125

                    ColumnLayout {

                        ExclusiveGroup {
                            id: dataType
                        }

                        RadioButton {
                            // id
                            id: microarray
                            objectName: "microarray"

                            text: "Microarray"
                            exclusiveGroup: dataType

                            // default: Micro-array selected
                            // no genome group visible
                            // miRNA is checked
                            // Upload button is enabled
                            checked: true

                            onClicked:{
                                progName = "microarray";
                                Utils.update_data_type(progName);
                                Utils.reset_genome_versions();
                                Utils.clear_selected_files();
                                gv_.onMenu();
                            }
                        }

                        RadioButton {
                            // id
                            id: rnaseq
                            objectName: "rnaseq"

                            text: "RNA-Seq"
                            exclusiveGroup: dataType
                            onClicked:{
                                progName = "rnaseq";
                                Utils.update_data_type(progName);
                                Utils.reset_genome_versions();
                                Utils.clear_selected_files();
                                gv_.onMenu();
                            }
                        }

                        RadioButton {
                            // id
                            id: chipseq
                            objectName: "chipseq"

                            text: "Chip-Seq"
                            exclusiveGroup: dataType
                            onClicked:{
                                progName = "chipseq";
                                Utils.update_data_type(progName);
                                Utils.reset_genome_versions();
                                Utils.clear_selected_files();
                                gv_.onMenu();
                            }
                        }

                        CheckBox {
                            // id
                            id: microRNACheckbox
                            objectName: "microRNACheckbox"


                            //text: qsTr("Breakfast")
                            text: qsTr("Use miRNA")

                            // description
                            checked: true

                            // event handling
                            onClicked: {
                                if (microRNACheckbox.checked == true)
                                {
                                    matrixName = "SwissReg_TargetScan"
                                    //matrixName = "1"
                                }
                                else
                                {
                                    matrixName = "SwissReg"
                                    //matrixName = "1"
                                }
                                console.log("Matrix name is: " + matrixName)
                            }
                        }
                    }
                }
            }
        }
        /* this functions toggles the menu and starts the animation */
        function onMenu()
        {
            if(progName != "microarray"){

                if(!gv_.menu_shown) {

                    genomeGroupList.opacity = 1
                    //slide_menu.x = 140
                    //gv_.width =  280
                    //console.log("sliding menu to "+(gv_.width * 0.4))
                    console.log("sliding menu to "+"280")
                }

                gv_.menu_shown = true

            } else {

                if(gv_.menu_shown) {

                    genomeGroupList.opacity = 0.3
                    //slide_menu.x = 100
                    //gv_.width =  150
                    console.log("sliding menu to "+"100")
                }
                gv_.menu_shown = false
            }
            //gv_.menu_shown = !gv_.menu_shown;
        }
    }
}
