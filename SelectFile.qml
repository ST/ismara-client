import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.3
import QtQuick.Dialogs 1.2

RowLayout {
    // id
    id: uploadLayout

    // positioning
    anchors.horizontalCenter: parent.horizontalCenter

    Button {
        // id
        id: uploadButton

        text: "  Upload files"

        // Styling
        // Copyright?
        iconSource: "https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/26/upload.png"

        style: ButtonStyle {
            background: Rectangle {
                implicitWidth: 150
                implicitHeight: 50
                border.width: control.activeFocus ? 2 : 1
                border.color: "black"
                radius: 4
                gradient: Gradient {
                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                    GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                }
            }
        }

        onClicked: {
            uploadOK = true
            openFileDialog()
        }

        function openFileDialog()
        {
            if (progName == 'microarray')
            {
                // no filter for microarray because
                // filters are not compatible with
                // selectFolder: true
                // (the Application crashes!)
                // the microarray.sh script should
                // check whether all the files in the dir
                // are of CEL format
                fileDialog.setSelectFolder(true);
                fileDialog.setSelectMultiple(false);
            }
            else
            {
                fileDialog.setSelectFolder(false);
                fileDialog.setSelectMultiple(true);
                // BED, BAM and SAM files only
                // if for whatever reason, the user renamed his/her files
                // then he/she should be given the possibility to view all the files
                // This implies checks in the upcoming steps
                fileDialog.setNameFilters(["BED, BAM, SAM files (*.bed* *.bam* *.sam*)", "All files (*.*)"]);
                fileDialog.selectedNameFilterIndex = 0;
            }
            fileDialog.open();
        }
    }

    Button {
        // id
        id: uploadClearButton

        text: "Clear"

        // event handling
        onClicked: clear()

        function clear()
        {
            fileDialog.list = ""
            fileUrls = ""
            outputDisplayText.text = ""
            processDataPanel.panelEnabled = false
        }
    }

    FileDialog {
        // id
        id: fileDialog

        // event handling
        onAccepted: {
            outputDisplayText.append("<b>Files or directory to be processed:</b>")
            //var paths = "Files or directory to be processed: \n"
            var list = ""
            fileCount = fileUrls.length
            for (var i = 0; i < fileUrls.length; ++i)
            {
                //paths += fileUrls[i].replace("file://","")
                //paths += "\n\n"

                outputDisplayText.append(fileUrls[i].replace("file://",""))

                list += fileUrls[i].replace("file://","")
                list += " "
            }
            //outputDisplayText.append(paths)
            processDataPanel.panelEnabled = true

            fileList = list
        }
        onRejected: { }
    }
}
