#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    QCoreApplication::setOrganizationName("sib");
    app.setApplicationName("ismara");

    // get the applications dir path and expose it to QML
    QUrl appPath(QString("%1").arg(app.applicationDirPath()));
    engine.rootContext()->setContextProperty("appPath", appPath);

    // Get the QStandardPaths home location and expose it to QML
    QUrl userPath;
    const QStringList usersLocation = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    if (usersLocation.isEmpty())
        userPath = appPath.resolved(QUrl("/home/"));
    else
        userPath = QString("%1").arg(usersLocation.first());
    engine.rootContext()->setContextProperty("userPath", userPath);

    QUrl imagePath;
    const QStringList picturesLocation = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation);
    if (picturesLocation.isEmpty())
        imagePath = appPath.resolved(QUrl("images"));
    else
        imagePath = QString("%1").arg(picturesLocation.first());
    engine.rootContext()->setContextProperty("imagePath", imagePath);

    QUrl videoPath;
    const QStringList moviesLocation = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation);
    if (moviesLocation.isEmpty())
        videoPath = appPath.resolved(QUrl("./"));
    else
        videoPath = QString("%1").arg(moviesLocation.first());
    engine.rootContext()->setContextProperty("videoPath", videoPath);

    QUrl homePath;
    const QStringList homesLocation = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    if (homesLocation.isEmpty())
        homePath = appPath.resolved(QUrl("/"));
    else
        homePath = QString("%1").arg(homesLocation.first());
    engine.rootContext()->setContextProperty("homePath", homePath);

    QUrl desktopPath;
    const QStringList desktopsLocation = QStandardPaths::standardLocations(QStandardPaths::DesktopLocation);
    if (desktopsLocation.isEmpty())
        desktopPath = appPath.resolved(QUrl("/"));
    else
        desktopPath = QString("%1").arg(desktopsLocation.first());
    engine.rootContext()->setContextProperty("desktopPath", desktopPath);

    QUrl docPath;
    const QStringList docsLocation = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
    if (docsLocation.isEmpty())
        docPath = appPath.resolved(QUrl("/"));
    else
        docPath = QString("%1").arg(docsLocation.first());
    engine.rootContext()->setContextProperty("docPath", docPath);


    QUrl tempPath;
    const QStringList tempsLocation = QStandardPaths::standardLocations(QStandardPaths::TempLocation);
    if (tempsLocation.isEmpty())
        tempPath = appPath.resolved(QUrl("/"));
    else
        tempPath = QString("%1").arg(tempsLocation.first());
    engine.rootContext()->setContextProperty("tempPath", tempPath);


    // custom location of the offline storage
    QString customPath = docPath.toString() + "/ismaraDB";
    QDir dir;
    if(dir.mkpath(QString(customPath))){
        engine.setOfflineStoragePath(QString(customPath));
    }

    // Replaced main.qml by Main.qml -> now a component
    // In order to set up integration tests
    engine.load(QUrl(QStringLiteral("qrc:/Main.qml")));

    return app.exec();
}
