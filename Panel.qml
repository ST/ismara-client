import QtQuick 2.0

Rectangle {
    id: panel

    property alias text: panelTitle.text
    property alias panelWidth: panel.width
    property alias panelHeigth: panel.height
    property alias panelEnabled: panel.enabled

    x: 10
    border.color: "lightgrey"
    border.width: 2
    radius: 4

    Rectangle {
        id: panelTitleContainer
        width: 100
        height: 10

        Text {
            id: panelTitle
            anchors.verticalCenter: parent.top
        }
    }
}

