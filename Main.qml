import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtProcess 0.1
import HttpUploader 0.1
import QtQuick.LocalStorage 2.0
import Qt.labs.settings 1.0
import QtQml 2.2

import "component-utils.js" as Utils

ApplicationWindow {

    id: app
    title: qsTr("ISMARA client")

    width: 820
    height: 700

    minimumWidth: 820
    minimumHeight: 700

    maximumWidth: 1200
    maximumHeight: 900

    visible: true

    SystemPalette{
        id: palette
        colorGroup: SystemPalette.Active
    }

    //color: palette.window

    // Global variables
    // USE x.y.z (single digits) and nothing else
    property string version:       "1.1.2"
    property string updateVersion:       ""
    //property string versionCheckServer: "https://ismara.unibas.ch/ISMARA/client/installers/"
    property string versionCheckServer: "http://ismara.unibas.ch/fcgi/client_version_check"
    property string updateMessage: ""
    property string programTitle:   "ISMARA client"
    property string progName:       "microarray"
    property string genomeName:     "hg19"
    property string matrixName:     "SwissReg_TargetScan"
    property string expressionFile: "expression.tab"
    property string fileList:       ""
    property string emailValue:     ""
    property string projectName:    ""
    property string resultPath:     ""
    property string uploadServer: "http://ismara.unibas.ch/fcgi/mara/client"
    property string faqUrl:       "http://mara.unibas.ch/MARA/FAQ/faq.html"
    property string contactUrl:       "mailto: helpdesk@expasy.org"+"?subject=[ISMARA]"
    property string resultUrl:    ""
    property string resultText: ""
    property double uploadProgress: 0
    property string uploadedState:  ""
    property bool uploadOK: true
    property string scriptOutput: ""
    property string standardMsgJson: ""
    property string errorMsgJson: ""
    property string colorMsg: "steelblue"
    property string colorError: "red"
    property string colorDefault: "steelblue"
    property real fileCount: 0
    property real progress_total: 0
    property var flag: new Object({filesareOK: false})

    // path settings
    property string jobid: "" // Issue #41
    property string rootPath: appPath
    property alias dataPath: settings.dataPath
    property alias inputPath: settings.inputPath
    property alias tmpPath: settings.tmpPath // system temp path
    property alias utilsPath: settings.utilsPath
    property alias rPath: settings.rPath
    property alias pythonPath: settings.pythonPath
    property alias dataOutput: settings.dataOutput
    property alias logPath: settings.logPath
    property alias scriptPath: settings.scriptPath


    // defaults when no other value available
    property string trootPath: appPath
    property string tdataPath: "data/"
    property string tinputPath: "data/src/"
    property string ttmpPath: tempPath // system temp path
    property string tutilsPath: "utils/"
    property string trPath: "scripts/R_libs/"
    property string tpythonPath: "scripts/python_libs/"
    property string tdataOutput: "data/output/"
    property string tlogPath: "logs/"
    property string tscriptPath: "scripts/"
    property string paths: ""

    //used only for the upload section
    property string uploadedGenomeName: ""

    property real miRna_flag: 0

    property var db; // local storage of job info

    property variant win;  // you can hold this as a reference..

    Settings {
        id: settings

        property string uploadServer: "http://ismara.unibas.ch/fcgi/mara/client"
        property alias emailValue: app.emailValue
        property string dataPath: "data/"
        property string inputPath: "data/src/"
        property string tmpPath: tempPath // system temp path
        property string utilsPath: "utils/"
        property string rPath: "scripts/R_libs/"
        property string pythonPath: "scripts/python_libs/"
        property string dataOutput: "data/output/"
        property string logPath: "logs/"
        property string scriptPath: "scripts/"

        onEmailValueChanged: console.log("Email changed to "+emailValue)

    }

    function storeSettings() { // executed maybe on destruction
        if(logPathSetting.length < 1) {
            settings.logPath = app.tlogPath
        } else {
            settings.logPath = logPathSetting.text
        }

        if(pythonPathSetting.length < 1) {
            settings.pythonPath = app.tpythonPath
        } else {
            settings.pythonPath = pythonPathSetting.text
        }

        if(rPathSetting.length < 1) {
            console.log("length = 0: "+rPathSetting.length)
            settings.rPath = app.trPath
        } else {
            console.log("length > 0: "+rPathSetting.length)
            settings.rPath = rPathSetting.text
        }

        if(tmpPathSetting.length < 1) {
            settings.tmpPath = app.ttmpPath
        } else {
            settings.tmpPath = tmpPathSetting.text
        }

        if(dataOutputSetting.length < 1) {
            settings.dataOutput = app.tdataOutput
        } else {
            settings.dataOutput = dataOutputSetting.text
        }

        console.log("After save, dataoutput: "+dataOutput)
        console.log("After save, settings.dataoutput: "+settings.dataOutput)
        console.log("After save, app.dataoutput: "+app.dataOutput)

    }

    function resetSettings() { // executed maybe on destruction
        logPathSetting.text = settings.logPath
        pythonPathSetting.text = settings.pythonPath
        rPathSetting.text = settings.rPath
        tmpPathSetting.text = settings.tmpPath
        dataOutputSetting.text = settings.dataOutput
        app.emailValue = settings.emailValue
    }


    Component.onCompleted: {
        console.log("start appPath: "+appPath)
        console.log("start tmpPath: "+settings.tmpPath)
        console.log("start dataOutput: "+settings.dataOutput)
        console.log("start rPath: "+settings.rPath)

        flag = new Object({filesareOK: true})

        resetSettings()

        // check if we have settings and make default ones if missing
        process_start.start()
    }

    Component.onDestruction: {

        console.log("Closing")
        if(process.isRunning) {
            console.log("job was running, must do cleanup")
            uploadOK = false
            process.kill()
            theUploader.abort()
            Utils.update_job(jobid, projectName, "Job stopped", "", "")

        }

        /*
        settings.dataOutput = app.dataOutput
        settings.emailValue = app.emailValue
        settings.logPath = app.logPath
        settings.pythonPath = app.pythonPath
        settings.rPath = app.rPath
        settings.tmpPath = app.tmpPath
*/
    }

    Process {

        id: process_start

        program: appPath+'/scripts/file_check.sh'

        onStarted: {
            uploadProgress = 0
            console.log("checking files and languages")
            console.log("processing: "+appPath+"/scripts/file_check.sh")
        }

        onReadyReadStandardOutput: {

            var infoTextArea = Utils.getInfoTextAreaObject()
            app.standardMsgJson = readAllStandardOutput()
            colorMsg = colorDefault
            console.log("filecheck raw: "+app.standardMsgJson)
            var jsonObject = tryParseJSON(app.standardMsgJson)
            var msg = "";
            if(jsonObject){

                if(jsonObject.severity == 1 && jsonObject.type == "info") {
                    console.log("found info message")
                    msg = jsonObject.msg;
                    infoTextArea.append(msg); // information window
                    tabInfo.copyBtn.enabled = true
                } else if(jsonObject.severity == 2 && jsonObject.type == "info") {
                    msg = jsonObject.msg;
                    infoTextArea.append(msg); // information window
                    resultText = msg; // result line
                }
            } else {
                console.error("got non json to standardoutput")
            }

        }

        onReadyReadStandardError: {

            var infoTextArea = Utils.getInfoTextAreaObject()
            app.errorMsgJson = readAllStandardError()
            console.log("filecheck raw error: "+app.errordMsgJson)
            var jsonObject = tryParseJSON(app.errorMsgJson)

            if(jsonObject){

                if (jsonObject.type == 'error') // If the error type is a real error!!
                {
                    // we had an error, don't enable anything, a popup maybe?
                    var error = "<font color='red'><strong>ERROR:</strong> "+jsonObject.msg+"</font>";
                    infoTextArea.append(error) // information window
                    resultText = error // result line

                    flag = new Object({filesareOK: false})
                    console.log("app.filesareOK set to false")
                    //processButton.disableAll()

                }
            }

        }

        onFinished: {

        }

    }


    Process {

        id: process

        // Never reset when a job is re-sumitted
        // jobid must be a global variable
        // and must be reset when clicking on the Process button
        //property string jobid: progName + '-' + Date.now()
        property bool isRunning: false

        property string paths: settings.logPath+"@"+settings.pythonPath+"@"+settings.rPath+"@"+settings.dataOutput+"@"+settings.tmpPath

        onStarted: {

            uploadOK = false

            console.log("process parameters: "+genomeName+" "+matrixName+" "+jobid+" "+paths+" "+fileList)
            console.log("jobid when starting: "+jobid)
            isRunning = true
            rootTimer.stop()
            resultText = "Starting process..."
            progress_total = 0
            uploadProgress = 0
            uploadedState = "Job is Running"
            elapsedTimer.running = true;
            rootTimer.visible = true;
            console.log("reset progress")

            var infoTextArea = Utils.getInfoTextAreaObject()

            var msg = new Date().toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss") + " Job is starting";
            infoTextArea.append(msg) // information window

        }

        program: appPath+'/scripts/'+progName+'.sh'
        arguments: [genomeName, matrixName, jobid, paths, fileList]

        onReadyReadStandardOutput: {

            var infoTextArea = Utils.getInfoTextAreaObject()

            app.standardMsgJson = readAllStandardOutput()

            console.log("stdoutput RAW: "+app.standardMsgJson)

            colorMsg = colorDefault

            // split it by #

            var msgJsonArr = app.standardMsgJson.split("#");

            // loop through
            for (var i = 0; i < msgJsonArr.length; i++) {
                console.log("processing: "+msgJsonArr[i]);
                var msgString = msgJsonArr[i].trim();

                // does it start with {, also json messages need to be more than 8 characters
                if(msgString[0] == '{' && msgString.length > 8){

                    var jsonObject = tryParseJSON(msgString)
                    var msg = "";

                    if(jsonObject.severity == 1 && jsonObject.type == "info") {
                        //console.log("found info message")
                        msg = jsonObject.msg;
                        infoTextArea.append(msg); // information window
                    } else if(jsonObject.severity == 2 && jsonObject.type == "info") {
                        msg = jsonObject.msg;
                        infoTextArea.append(msg); // information window
                        resultText = msg; // result line
                    } else if (jsonObject.type == "progress_total"){
                        // 5 steps or progress
                        //console.log("fileurl length: "+fileCount)
                        progress_total = jsonObject.msg
                    } else if (jsonObject.type == "progress" && jsonObject.severity == 2){
                        // 5 steps or progress
                        //console.log("fileurl length: "+fileCount)
                        uploadProgress = uploadProgress + (jsonObject.msg / progress_total)
                    } else if(jsonObject.severity == 1 && jsonObject.type == "chip_id") {
                        expressionFile = jsonObject.msg
                    } else if(jsonObject.severity == 1 && jsonObject.type == "success") {
                        uploadOK = true
                    }

                } else {

                    console.log("msgString is standard raw: "+msgString)
                    if(msgString.length > 0 && msgString[0] != '['){
                        infoTextArea.append(msgString); // information window
                    }

                }

            }

        }
        onReadyReadStandardError: {

            var infoTextArea = Utils.getInfoTextAreaObject()

            //console.log("1st errinput"+readAllStandardError())
            app.errorMsgJson = readAllStandardError()
            //colorMsg = colorError

            console.log("stderror RAW: "+app.errorMsgJson)
            //var patt = new RegExp(/msg/);
            //if(patt.test(app.errorMsgJson)) {
            var jsonObject = tryParseJSON(app.errorMsgJson)

            var msg = "";
            if(jsonObject){
                //var jsonObject = JSON.parse(app.errorMsgJson)
                //console.log("is error json: "+app.errorMsgJson)
                //console.log("msg parsed to: "+jsonObject.msg)
                if (jsonObject.type == 'error') // If the error type is a real error!!
                {
                    //console.log("found error message")
                    // For instance, R outputs DEBUG messages on STDERR!
                    // That's why we now set the flag to
                    // false only when the JSON object's type is error.
                    uploadOK = false
                    msg = "<font color='red'><strong>ERROR:</strong> "+jsonObject.msg+"</font>"
                    infoTextArea.append(msg) // information window
                    console.log("setting resultText; "+msg)
                    resultText = msg // result line

                    Utils.update_job(jobid, projectName, "Job failed", "", "")

                    process.kill();
                    uploadedState = "Job is stopped"
                    msg = new Date().toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss") + " Job has failed";
                    infoTextArea.append(msg) // information window
                    elapsedTimer.running = false;
                    elapsedTimer.stop();
                    isRunning = false
                }
            } else {

                // for now don't show plain text errors as the scripts output "stuff" there that aren't necessary errors
                //console.log("is error raw: "+app.errorMsgJson)
                //resultText = "<font color='red'><strong>ERROR:</strong> "+app.errorMsgJson+"</font>"
                //resultText = app.errorMsgJson

            }
            //colorMsg = colorDefault


        }

        onFinished: {

            var infoTextArea = Utils.getInfoTextAreaObject()

            isRunning = false
            uploadedState = ""

            var msg = "";

            if(uploadOK) {
                // as we use the same upload progress bar for job progress for now
                uploadProgress = 0
                resultText = "Preparing to upload"
                console.log("Calling upload "+uploadServer)

                theUploader.clear()

                theUploader.open(uploadServer);

                uploadedGenomeName = genomeName
                // modify genomeName to comply with ISMARA web service
                if(genomeName == "mm10"){
                    uploadedGenomeName = "mm10_f5"
                }
                if(genomeName == "hg19"){
                    uploadedGenomeName = "hg19_f5"
                }

                theUploader.addField("address", emailValue);
                theUploader.addField("project", projectName)
                theUploader.addField("dataType", progName);
                theUploader.addField("genomeID", uploadedGenomeName);

                if(matrixName == "SwissReg_TargetScan"){
                    theUploader.addField("useMirna", 1);
                    miRna_flag = 1

                } else {
                    theUploader.addField("useMirna", 0);
                    miRna_flag = 0
                }

                //resultPath = appPath + "/data/output/" + app.jobid + "/" + expressionFile;

                if(dataOutput.toString().slice(-1) != "/"){
                    dataOutput = dataOutput+"/"
                }

                if(dataOutput.toString().charAt(0) === "/"){
                    resultPath = dataOutput + app.jobid + "/" + expressionFile;
                } else {
                    resultPath = appPath + "/" + dataOutput + app.jobid + "/" + expressionFile;
                }

                console.log("resultpath: "+resultPath)
                theUploader.addFile("filedata", resultPath);

                msg = "File to upload: " + resultPath;
                infoTextArea.append(msg) // information window

                msg = "Parameters: address=" + emailValue + " ; project=" + projectName + " ; dataType=" + progName + " ; genomeID=" + uploadedGenomeName + " ; useMirna=" + miRna_flag
                infoTextArea.append(msg) // information window

                Utils.update_job(jobid, projectName, "Uploading", "", "")

                msg = new Date().toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss") + " uploading is starting";
                infoTextArea.append(msg) // information window

                theUploader.send()

            } else {

                process.kill()
                infoTextArea.append("<br><br>Error when processing files, result file not created!") // information window
                //resultText = "" // result line
                theUploader.abort()

                // update should not be the last step otherwise the commit is not done
                //Utils.update_job(jobid, projectName, "Job failed", "", "")
                Utils.renableForUpcomingAnalysis()
                tabInfo.copyBtn.enabled = true
            }
            elapsedTimer.running = false;
            elapsedTimer.stop();
        }

    }

    HttpUploader {
        id: theUploader

        onUploadStateChanged: {

            var infoTextArea = Utils.getInfoTextAreaObject()
            var msg = "";
            if( uploadState == HttpUploader.Done) {

                if(status == 200) {

                    console.log("Upload done with status " + status);
                    uploadedState = "Done"
                    elapsedTimer.running = false;
                    elapsedTimer.stop();

                    // result is plain text for now
                    //var jsonObject = JSON.parse(responseText);
                    var jsonObject = tryParseJSON(responseText)

                    msg = "Server message: " + jsonObject[0].message + ", server status: "+jsonObject[0].status;
                    infoTextArea.append(msg); // information window
                    resultText = 'Result: <a href="'+jsonObject[0].url+'">'+jsonObject[0].url+'</a>'; // result line

                    msg = new Date().toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss") + " Upload done.";
                    infoTextArea.append(msg) // information window


                    var someFormattedDate = Utils.get_expiration_date();
                    Utils.update_job(jobid, projectName, "Done", "<a href='" + jsonObject[0].url + "'>Click</a>", someFormattedDate)
                    Utils.renableForUpcomingAnalysis()
                    tabInfo.copyBtn.enabled = true

                } else if (status == 0) {
                    msg = new Date().toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss") + "Upload failed, please check network connection or server availability, http server status is "+status
                    infoTextArea.append(msg) // information window
                    resultText = "<font color='red'><strong>ERROR:</strong> " + msg + "</font>" // result line

                    Utils.update_job(jobid, projectName, "Upload failed", "", "")
                    Utils.renableForUpcomingAnalysis()
                    tabInfo.copyBtn.enabled = true
                }
            } else {
                console.log("Upload state :"  + uploadState)
                if(errorString){
                    console.log("Error with upload: "+errorString)
                }
            }

        }

        onProgressChanged: {
            console.log("Upload progress = " + progress)
            uploadProgress = progress
            uploadedState = status
        }
    }

    Rectangle {

        id: root
        anchors.fill: parent
        antialiasing : true
        border.color: "red"
        border.width: 1
        radius: 5

        Component.onCompleted: {
            //console.log("root width: "+root.width)

        }

        //Column
        Rectangle
        {
            id: container
            anchors.fill: parent

            width: app.width
            height: app.height


            Component.onCompleted: {
                //console.log("app width: "+app.width)
                //console.log("app height: "+app.height)
            }


            Title {
                id: titleRect
                width: parent.width ;
                height: 100;
                anchors.top: parent.top
            }

            ChooseOption {
                id: chooseOptions;
                objectName: "chooseOptions"
                anchors.top: titleRect.bottom
            }
            Parameter {
                id: parameter
                anchors.top: chooseOptions.bottom

            }

            Rectangle {
                id: resultBox

                width: parent.width
                visible: true
                height: 35
                anchors.top: parameter.bottom


                Component.onCompleted: {
                    //console.log("app width: "+app.width)
                    //console.log("app height: "+app.height)
                }


                BusyIndicator
                {
                    id:busy1
                    x: 40
                    width:15
                    height:15
                    visible: process.isRunning
                    running: process.isRunning
                }

                Text {
                    id: link_Text
                    text: resultText
                    fontSizeMode: Text.Fit; minimumPixelSize: 8; font.pixelSize: 12
                    textFormat: Text.RichText
                    color: colorMsg
                    clip:true
                    width: parent.width
                    wrapMode: Text.WordWrap
                    anchors.verticalCenter: busy1.verticalCenter
                    anchors.left: process.isRunning ? busy1.right : busy1.left
                    anchors.leftMargin: 10

                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.NoButton // we don't want to eat clicks on the Text
                        cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                    }

                    onLinkActivated: {
                        Qt.openUrlExternally(link)
                    }
                }
            }


            Rectangle {

                //width: app.width - 20
                height: app.height - resultBox.bottom - 35
                x: 40
                anchors.top: resultBox.bottom

                //color: "red"

                //JobsAndLogs { id: tabContainer }
                /* following code is from JobsAndLogs which would be better place
                  but as the js functions don't get tabJobs it's inline here instead
                  try to fix this somehow
                  */

                Rectangle {
                    id: tabContainer
                    objectName: "tabContainer"

                    width: app.width - 65
                    height: app.height - (resultBox.y + resultBox.height) - 80

                    anchors.margins: 30

                    Component.onCompleted: {
                        //console.log("tabContainer parent height: "+parent.width)
                        //console.log("tabContainer app height: "+app.width)
                        //console.log("tabContainer app height: "+app.height)
                        //console.log("tabContainer resultbox y : "+resultBox.y)
                        //console.log("tabContainer box height : "+(app.height - (resultBox.y + resultBox.height) - 180))
                    }


                    TabView {

                        id: tabView
                        width: parent.width
                        height: parent.height
                        property int margins: Qt.platform.os === "osx" ? 16 : 0

                        Tab {
                            id: tabJobs
                            title: "Jobs"


                            // Lazy loading!
                            active: true

                            //Tab turns out to behave like a Loader, loading whichever Component you give it;
                            // thus, the textArea component is instantiated by the Tab in a *separate QML context*
                            // i.e parented to that of the document root item!!!
                            // Everything inside that context can see things up the scope chain, *but not the reverse*!!!
                            property Item jobTable: item.jobTable

                            Rectangle {
                                id: tableRect

                                // jobTable property is assigned TableView component
                                // in order to be "visible" in the Tab context (and later on in JS)
                                // see explanation about the loading in Tabs above
                                property Item jobTable: jobTable

                                ListModel {
                                    id: jobModel
                                }

                                Component {
                                    id: clearDelegate
                                    Button {
                                        text: "Clear"

                                        style: ButtonStyle {
                                            background: Rectangle {
                                                implicitWidth: 60
                                                implicitHeight: 18
                                                border.width: control.activeFocus ? 1 : 1
                                                border.color: "#888"
                                                radius: 4
                                                gradient: Gradient {
                                                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                                    GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                                                }
                                            }
                                        }

                                        onClicked: Utils.remove_job(index)
                                    }
                                }

                                Component {
                                    id: emptyDelegate
                                    Button {
                                        id: emptyButton
                                        visible: false
                                    }
                                }

                                Component {
                                    id: btnDelegate
                                    Button {
                                        id: actionBtn

                                        text: "Stop"

                                        style: ButtonStyle {
                                            background: Rectangle {
                                                implicitWidth: 60
                                                implicitHeight: 16
                                                border.width: 1
                                                border.color: "#888"
                                                radius: 4
                                                gradient: Gradient {
                                                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                                    GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                                                }
                                            }
                                        }

                                        // event handling
                                        onClicked: stop()

                                        function stop()
                                        {
                                            //console.log("clicked stop")
                                            uploadOK = false
                                            process.kill()
                                            theUploader.abort()
                                            Utils.update_job(jobid, projectName, "Job stopped", "", "")
                                            var msg = new Date().toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss") + " Job stopped by the user";
                                            infoTextArea.append(msg) // information window

                                        }
                                    }
                                }

                                Component {
                                    id: textDelegate

                                    Text {
                                        // would be nice to know if there's a way to add margin inside table cell
                                        // anchors etc. tested but did not work
                                        text: "  "+itemText+"  "
                                        textFormat: Text.RichText

                                        height: 18

                                        MouseArea {
                                            anchors.fill: parent
                                            acceptedButtons: Qt.NoButton // we don't want to eat clicks on the Text
                                            cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                                        }
                                        onLinkActivated: {
                                            Qt.openUrlExternally(link)
                                        }
                                    }

                                }
                                TableView {
                                    id: jobTable

                                    frameVisible: false
                                    anchors.fill: parent

                                    TableViewColumn {
                                        role: "project"
                                        title: "Project"
                                        width: 180

                                        //elideMode: Text.ElideMiddle
                                        //resizable: false
                                    }
                                    TableViewColumn {
                                        role: "identifier"
                                        title: "Job identifier"
                                        width: 180

                                    }
                                    TableViewColumn {
                                        role: "status"
                                        title: "Job status"
                                        width: 90
                                        //resizable: false
                                    }
                                    TableViewColumn {
                                        role: "url"
                                        title: "Job URL"
                                        width: 80
                                        //resizable: false
                                    }


                                    TableViewColumn {
                                        role: "expiration"
                                        title: "Available until"
                                        width: 110
                                        //resizable: false
                                    }


                                    TableViewColumn {
                                        role: "action"
                                        title: "Action"
                                        width: 64
                                        resizable: false


                                    }

                                    model:jobModel
                                    itemDelegate: Item {
                                        clip: true
                                        anchors.leftMargin: 10

                                        Loader {
                                            property string itemText: typeof (styleData.value) == 'undefined' ? "":styleData.value
                                            property int index: styleData.row
                                            //property int width: font.textWidth(itemText)
                                            // Action column consist of a button:
                                            // Stop button when "Running" or "Uploading"
                                            // Clear button when the job is done, stopped or has failed.
                                            sourceComponent: getDelegate()


                                            function getDelegate()
                                            {
                                                if (styleData.column === 5)
                                                {
                                                    if (typeof (jobModel.get(styleData.row)) !== 'undefined')
                                                    {
                                                        var jobStatus = jobModel.get(styleData.row)["status"]
                                                        console.log("in getDelegate jobStatus is "+jobStatus)
                                                        if (typeof(jobStatus) !== 'undefined')
                                                        {
                                                            if (jobStatus === "Running" || jobStatus === "Uploading")
                                                            {
                                                                console.log("running or Uploading in getDelegate jobStatus is "+jobStatus)
                                                                return btnDelegate;
                                                            }
                                                            else if (jobStatus === "Done" || jobStatus === "Job stopped" || jobStatus === "Job failed" || jobStatus === "Upload failed" || jobStatus.indexOf("Stop") > -1)
                                                            {
                                                                console.log("Done or job Stoppep of Job failed in getDelegate jobStatus is "+jobStatus)
                                                                return clearDelegate;
                                                            }

                                                        }

                                                    }

                                                }
                                                else
                                                {
                                                    return textDelegate;
                                                }

                                                return emptyDelegate;

                                            }

                                        }

                                    }
                                    rowDelegate: Item {
                                        height: 24

                                    }
                                    headerDelegate: Rectangle {
                                        height: textItem.implicitHeight * 1.2
                                        width: textItem.implicitWidth
                                        color: "#f5f5f5"
                                        border.color: "lightgrey"
                                        Text {
                                            id: textItem
                                            anchors.fill: parent
                                            verticalAlignment: Text.AlignVCenter
                                            horizontalAlignment: styleData.textAlignment
                                            anchors.leftMargin: 10
                                            text: styleData.value
                                            font.pixelSize: 14
                                            elide: Text.ElideRight
                                            renderType: Text.NativeRendering
                                            anchors.verticalCenter: parent.verticalCenter

                                        }
                                        Rectangle {
                                            anchors.right: parent.right
                                            anchors.top: parent.top
                                            anchors.bottom: parent.bottom
                                            width: 1
                                            color: "lightgrey"
                                        }
                                    }
                                }
                            }
                        }

                        Tab {
                            id: tabInfo
                            title: "Information"
                            // Lazy loading
                            // Force the loading in order
                            // to append the Json msg to the textarea!
                            active: true

                            //Tab turns out to behave like a Loader, loading whichever Component you give it;
                            // thus, the textArea component is instantiated by the Tab in a *separate QML context*
                            // i.e parented to that of the document root item!!!
                            // Everything inside that context can see things up the scope chain, *but not the reverse*!!!
                            property Item textArea: item.textArea
                            property Item copyBtn: item.copyBtn


                            Rectangle {
                                // id
                                id: outputDisplay

                                // textArea property is aiigned outputdiplayText component
                                // in order to be "visible" in the Tab context (and later on in JS)
                                // see explanation about the loading in Tabs above
                                property Item textArea: outputDisplayText
                                property Item copyBtn: copyBtn

                                // Create a flickable to view a large text box.
                                Flickable {
                                    id: view
                                    anchors.fill: parent

                                    contentWidth: outputDisplay.width
                                    contentHeight: outputDisplay.height


                                    // Flickable does not automatically clip its contents. If it is not used as a full-screen item,
                                    // you should consider setting the clip property to true.
                                    clip: true
                                    TextArea {
                                        id: outputDisplayText
                                        width: parent.width-5
                                        height: parent.height-5
                                        anchors.margins: 4
                                        wrapMode: TextEdit.WordWrap
                                        textMargin: 10
                                        textFormat: TextEdit.RichText
                                        textColor: colorMsg
                                        text: ""
                                        font.family: "Helvetica";
                                        font.pixelSize: 12
                                        backgroundVisible: false
                                    }
                                    Button {
                                        id: copyBtn
                                        text: "Copy to clipboard"

                                        anchors.right: parent.right
                                        anchors.rightMargin: 20
                                        anchors.top: parent.top
                                        anchors.topMargin: 10

                                        enabled: false

                                        onClicked: Utils.copy_to_clipboard();
                                    }

                                }
                            }
                        }
                    }


                }

                // end of tab rectangle

            }

        }

    }


    // menuBar + Action types
    // Menus and MenuItems still have to be discussed

    // For now, I was not able to put all this code in a component or whatever
    // Should give it another try!

    Window {
        id: settingsDialog
        width: settingsWarningRow.width + 40
        minimumWidth: 400
        height: 300

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 10

            RowLayout {
                id: settingsWarningRow
                Layout.fillWidth: true
                Layout.fillHeight: true
                spacing: 5

                Text {
                    id: settingsWarning
                    text: qsTr("<b>Warning:</b> Advanced use only<br/>Paths are prepended with:<br/><i>"+appPath+ "</i><br/>unless they start with a '/'<br/>To reset a value, clear a field and save.")
                }
            }

            RowLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                spacing: 10

                Text {
                    id: logPathlbl
                    text: "<b>Log Path:</b>"
                }

                TextField {
                    id: logPathSetting

                    anchors.verticalCenter: logPathlbl.verticalCenter
                    anchors.left: logPathlbl.right
                    anchors.leftMargin: 4
                    Layout.fillWidth: true
                    maximumLength: 400
                    width: 200
                    height: 22
                    text: logPath
                }

            }

            RowLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                spacing: 10

                Text {
                    id: pythonPathlbl
                    text: "<b>Python Lib Path:</b>"
                }

                TextField {
                    id: pythonPathSetting
                    anchors.verticalCenter: pythonPathlbl.verticalCenter
                    anchors.left: pythonPathlbl.right
                    anchors.leftMargin: 4
                    Layout.fillWidth: true
                    maximumLength: 400
                    width: 200
                    height: 22
                    text: pythonPath
                }

            }


            RowLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                spacing: 10

                Text {
                    id: rPathlbl
                    text: "<b>R Lib Path:</b>"
                }

                TextField {
                    id: rPathSetting
                    anchors.verticalCenter: rPathlbl.verticalCenter
                    anchors.left: rPathlbl.right
                    anchors.leftMargin: 4
                    Layout.fillWidth: true
                    maximumLength: 400
                    width: 200
                    height: 22
                    text: rPath
                }

            }


            RowLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                spacing: 10

                Text {
                    id: tmpPathlbl
                    text: "<b>Temp Path:</b>"
                }

                TextField {
                    id: tmpPathSetting

                    anchors.verticalCenter: tmpPathlbl.verticalCenter
                    anchors.left: tmpPathlbl.right
                    anchors.leftMargin: 4
                    Layout.fillWidth: true
                    maximumLength: 400
                    width: 200
                    height: 22
                    text: tmpPath
                }

            }

            RowLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                spacing: 10

                Text {
                    id: dataOutputlbl
                    text: "<b>Data output Path:</b>"
                }

                TextField {
                    id: dataOutputSetting
                    anchors.verticalCenter: dataOutputlbl.verticalCenter
                    anchors.left: dataOutputlbl.right
                    anchors.leftMargin: 4
                    Layout.fillWidth: true
                    maximumLength: 400
                    width: 200
                    height: 22
                    text: dataOutput
                }

            }

            Rectangle {
                height: 30
                Layout.fillWidth: true

                Button {
                    id: settingsSave
                    text: "Save"
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    onClicked: {
                        settingsDialog.hide()
                        storeSettings()
                    }
                }

                Button {
                    id: settingsCancel
                    text: "Cancel"
                    anchors.right: settingsSave.left
                    anchors.rightMargin: 10
                    onClicked: {
                        resetSettings()
                        settingsDialog.hide();
                    }
                }

            }
        }

    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            MenuItem {
                action: appSettings
                text: "Settings"
            }
            MenuItem { action: exit }
        }

        Menu {
            title: qsTr("&Help")


            MenuItem {
                text: "&User manual"
                onTriggered: {
                    var component = Qt.createComponent("UserManual.qml");
                    win = component.createObject(root);
                    win.show();
                }
            }

            MenuItem {
                text: "&FAQ"
                onTriggered: Qt.openUrlExternally(faqUrl)
            }


            MenuItem {
                text: "&Technical support"
                onTriggered: Qt.openUrlExternally(contactUrl)
            }


            MenuItem {
                text: "About "+app.programTitle
                onTriggered: aboutDialog.show()
            }
        }
    }

    Action {
        id: exit
        text: qsTr("E&xit")
        onTriggered: Qt.quit();
    }


    Action {

        id: appSettings
        text: qsTr("Setti&ngs")
        onTriggered: settingsDialog.show();
    }

    // End menuBar + Action types

    // Status bar. To be improved!
    statusBar: StatusBar {

        //RowLayout {
        //id: statusBarLayout
        //Layout.fillWidth: true
        //anchors.verticalCenter: parent.verticalCenter

        ProgressBar {
            id: mainProgress
            width: 200
            anchors.margins: 5
            height: 15
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left

            value: uploadProgress
            style: ProgressBarStyle {
                background: Rectangle {
                    radius: 2
                    color: "lightgray"
                    border.color: "gray"
                    border.width: 1
                    implicitWidth: 200
                    implicitHeight: 16
                }
                progress: Rectangle {
                    color: "lightsteelblue"
                    border.color: "steelblue"
                }
            }
        }

        Rectangle  {

            id: rootTimer
            anchors.right: parent.right
            anchors.rightMargin: 55
            // Layout.fillWidth: true
            Layout.minimumWidth: 40
            Layout.preferredWidth: 50
            //anchors.margins: 3
            height: 20
            anchors.verticalCenter: StatusBar.verticalCenter

            property double startTime: 0
            property int secondsElapsed: 0
            property string timeElapsed: ""

            function stop() {
                rootTimer.startTime = 0;
                elapsedTimer.running = false;
            }

            function restartCounter()  {

                rootTimer.startTime = 0;
                elapsedTimer.running = true;

            }

            function timeChanged()  {
                if(rootTimer.startTime==0)
                {
                    rootTimer.startTime = new Date().getTime(); //returns the number of milliseconds since the epoch (1970-01-01T00:00:00Z);
                }
                var currentTime = new Date().getTime();
                //rootTimer.secondsElapsed = (currentTime-startTime)/1000;
                //var hrs=Math.floor(rootTimer.secondsElapsed / 3600);
                //var mins=Math.floor(rootTimer.secondsElapsed - (hrs * 3600)/60);
                //var secs=rootTimer.secondsElapsed - (hours * 3600) - (minutes * 60)

                //rootTimer.timeElapsed=(hrs< 10 ? "0" + hrs : hrs) + ":" + (mins < 10 ? "0" + mins : mins) + ":" + (secs < 10 ? "0" + secs : secs);

                rootTimer.timeElapsed=new Date((currentTime-startTime)).toISOString().substr(11, 8);
            }

            Timer  {
                id: elapsedTimer
                interval: 1000;
                running: process.isRunning;
                repeat: true;
                onTriggered: rootTimer.timeChanged()
            }

            Text {
                id: counterText
                text: rootTimer.timeElapsed
            }

        }

        Item {
            Layout.fillWidth: true
            anchors.leftMargin: 10
            anchors.left: mainProgress.right
            width: 220
            //anchors.margins: 3
            height: 20
            anchors.verticalCenter: StatusBar.verticalCenter

            Text {
                id: uploadStateText
                text: qsTr(uploadedState)
            }

        }

        //}

    }

    function tryParseJSON (jsonString){
        if(jsonString.length > 2){
            try {
                var o = JSON.parse(jsonString);

                // console.log("> "+o.type+" = "+o.msg+" <");
                // Handle non-exception-throwing cases:
                // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
                // but... JSON.parse(null) returns 'null', and typeof null === "object",
                // so we must check for that, too.
                if (o && typeof o === "object" && o !== null) {
                    return o;
                }
            }
            catch (e) {
                console.log("Tried string is not a json string: \n"+jsonString)
                console.log("tryParseJSON error: "+e)
            }
        }
        return false;
    }


    // Local storage of job information
    // (SQLite database - See http://doc.qt.io/qt-5/qtquick-localstorage-qmlmodule.html for details)
    Item {
        Component.onCompleted: {
            console.log("init db, read jobinfo")
            db = Utils.init_database();
            Utils.read_job_info();

            //request(app.versionCheckServer+"version.json?version="+app.version, function (o) {
            request(app.versionCheckServer+"?version="+app.version, function (o) {

                if(o.responseText){
                    // log the json response
                    console.log(o.responseText);

                    // translate response into object
                    var d = eval('new Object(' + o.responseText + ')');

                    // access elements inside json object with dot notation
                    console.log("ver: "+d.version);
                    console.log("msg: "+d.message);

                    app.updateVersion = d.version;
                    app.updateMessage = d.message;

                    if(versionCompare(app.updateVersion, version) > 0){
                        console.log("showing version window");
                        versionDialog.show();

                    }
                }

            });

        }
    }

    About { id: aboutDialog }
    Version { id: versionDialog }
    NoInternet { id: nointernetDialog }

    function request(url, callback) {
        var xhr = new XMLHttpRequest();

        var timer = Qt.createQmlObject("import QtQuick 2.3; Timer {interval: 4000; repeat: false; running: true;}",root,"MyTimer");
        timer.triggered.connect(function(){
            xhr.abort();
            nointernetDialog.show();
        });

        xhr.onreadystatechange = (function(myxhr) {
            timer.running = false;
            return function() {
                if(myxhr.readyState === 4) callback(myxhr);
            }
        })(xhr);
        xhr.open('GET', url, true);
        xhr.send('');
    }

    function versionCompare (a, b) {

        console.log("comparing "+a+" to "+b);
        var i, l, diff, segmentsA, segmentsB;

        segmentsA = a.replace(/(\.0+)+$/, '').split('.');
        segmentsB = b.replace(/(\.0+)+$/, '').split('.');
        l = Math.min(segmentsA.length, segmentsB.length);

        for (i = 0; i < l; i++) {
            diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
            if (diff !== 0) {
                return diff;
            }
        }
        return segmentsA.length - segmentsB.length;
    }

}
