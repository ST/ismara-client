function copy_to_clipboard() {
    var textArea = getInfoTextAreaObject();
    // the copy() method copies the selected portion
    // of the text area. That's why we have to select
    // all its content before copying.
    // May be misleading for users.
    if (textArea.selectedText == "")
    {
        textArea.selectAll();
    }
    textArea.copy();
}

function delete_job_info(jobid) {

    if(!db) {
        console.log("database not available in delete_job_info");
        return;
    }

    db.transaction( function(tx) {
        tx.executeSql('DELETE FROM jobinfo WHERE job_id = ?', [ jobid ]);
    });
}

function update_job_info(jobid, status, url, expDate) {

    if(!db) {
        console.log("database not available in update_job_info");
        return;
    }

    db.transaction( function(tx) {
        tx.executeSql('UPDATE jobinfo SET status = ?,url = ?,expiration_date = ? WHERE job_id = ?', [ status, url, expDate, jobid]);
    });
}

function insert_job_info(projectName, jobid, status, url, expDate) {

    if(!db) {
        console.log("database not available in insert_job_info");
        return;
    }

    db.transaction( function(tx) {
        tx.executeSql('INSERT INTO jobinfo VALUES(?, ?, ?, ?, ?)', [ projectName, jobid, status, url, expDate ]);
    });

}

function read_job_info() {

    if(!db) {
        console.log("database not available in read_job_info");
        return;
    }

    db.transaction( function(tx) {
        console.log("getting jobs");
        var result = tx.executeSql('select * from jobinfo where status != "Running"');
        var model = get_model();

        if(result.rows.length > 0) {

            for(var i = 0; i < result.rows.length; i++) {
                //console.log("to model:");
                //console.log(result.rows.item(i).project_name);
                //console.log(result.rows.item(i).job_id);
                //console.log(result.rows.item(i).status);
                //console.log(result.rows.item(i).url);
                //console.log(result.rows.item(i).expiration_date);
                model.insert(i, {"project": result.rows.item(i).project_name, "identifier": result.rows.item(i).job_id, "status": result.rows.item(i).status, "url": result.rows.item(i).url, "expiration": result.rows.item(i).expiration_date});
            }

        } else {

            console.log("No jobs found");

        }
    });
}

function init_database() {
    var db = LocalStorage.openDatabaseSync("Ismara", "1.0", "ismara job information", 100000);
    db.transaction( function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS jobinfo(project_name TEXT, job_id TEXT, status TEXT, url TEXT, expiration_date TEXT)');
    });
    return db;
}

function get_identifier(model, index)
{
    return model.get(index).identifier;
}

function remove_job(index)
{
    var model = get_model();
    var dbid = get_identifier(model, index);
    model.remove(index);
    delete_job_info(dbid);
}

function get_expiration_date ()
{
    // expiration date = today's date + 15 days
    var expirationDate = new Date();
    var numberOfDaysToAdd = 15;
    expirationDate.setDate(expirationDate.getDate() + numberOfDaysToAdd);
    // format the date
    var dd = expirationDate.getDate();
    var mm = expirationDate.getMonth() + 1;
    var y = expirationDate.getFullYear();
    var someFormattedDate = dd + '/'+ mm + '/'+ y;

    return someFormattedDate;
}

function get_model()
{
    console.log("getting jobs model")
    var table = tabJobs.jobTable
    var model = table.model
    return model;
}

function get_count(model)
{
    return model.count;
}

function resize_projet_column(size)
{
    var table = tabJobs.jobTable
    var max = 200
    var min = 50
    var current = table.getColumn(0).width

    // column width between 50 and 250
    var fitted = size < min ? min : size > max ? max : size;
    // enlarge if feasible, do not narrow down the width
    fitted = fitted < current ? current : fitted;

    table.getColumn(0).width = fitted;
}

function addJob(jobid, projectName, status, url, expDate)
{
    var model = get_model();
    var count = get_count(model)

    model.insert(count, {"project": projectName, "identifier": jobid, "status": status, "url": url, "expiration": expDate})
    insert_job_info(projectName, jobid, status, url, expDate);
    //resize_projet_column(projectName.length * 10); // px vs string length
}

function update_job(jobid, projectName, status, url, expDate)
{
    console.log("updating jobid: "+jobid);
    console.log("updating projectName: "+projectName);
    console.log("updating status: "+status);
    console.log("updating url: "+url);
    console.log("updating expDate: "+expDate);
    var model = get_model();
    var count = get_count(model);

    model.set(count - 1, {"status": status, "url": url, "expiration": expDate});
    update_job_info(jobid, status, url, expDate); // to be implemented!
    console.log("syncing model")
    tabJobs.jobTable.model.clear()
    read_job_info()

}

function showRequestInfo(text) {
    //log.text = log.text + "\n" + text
    console.log(text)
}

function renableForUpcomingAnalysis ()
{
    chooseOptions.enabled = true
    parameter.enabled = true
    uploadProgress = 0
    expressionFile = "expression.tab"
    projectName = ""
    jobid = ""
}

function getInfoTextAreaObject()
{
    var textArea = tabInfo.textArea;
    return textArea;
}

function disableAll ()
{
    chooseOptions.enabled = false;
    parameter.enabled = false;
}

function reenable_file_selection()
{
    background.enabled = true
}

function reset_genome_versions() {
    // loop through the genome GroupBox instead?
    hg19.checked = false
    hg18.checked = false
    mm9.checked = false
    mm10.checked = false
}

function update_data_type(type) {

    if (type === 'rnaseq')
    {
        gv_.menu_shown = true
        genomeGroup.visible = true
        genomeGroupList.opacity = 1
        microRNACheckbox.checked = true
        matrixName = "SwissReg_TargetScan"
        background.enabled = false
    }
    else if (type === 'microarray') {
        gv_.menu_shown = false
        genomeGroup.visible = true
        genomeGroupList.opacity = 0.3
        microRNACheckbox.checked = true
        matrixName = "SwissReg_TargetScan"
        background.enabled = true
    }
    else if (type === 'chipseq') {
        gv_.menu_shown = true
        genomeGroup.visible = true
        genomeGroupList.opacity = 1
        microRNACheckbox.checked = false
        matrixName = "SwissReg"
        background.enabled = false
    }
}

function clear_selected_files() {

    nbFiles.text = ""

    fileChooserModel.remove(0);
    fileChooserModel.insert(0, {"deleteFlag":false, "files": "Please, import your data"});

    // Empty the ListView
    var nb_of_files = fileChooserModel.count
    for (var i = 1; i < nb_of_files; ++i)
    {
        // when a file is removed from the list
        // then all the files are shifted by 1 position
        // That's why we should always remove the 1st file
        fileChooserModel.remove(1)
    }

    // reset the global variable (used by the wrapper scripts)
    fileList = ""

    // disable the process button
    parameter.processEnabled = false;
}

function remove_file(index)
{
    // First remove the file from the list view
    fileChooserModel.remove(index);

    // then remove the file from the list of files
    var fileIndex = index - 1;
    var listAsArray = fileList.split(" ");

    // new file count = current file count minus 1
    var fileCount = listAsArray.length - 1

    if(fileCount < 1) {

        Utils.clear_selected_files();

    } else {

        /* One file remaining -> Disable the "Process data" button */
        if (fileCount == 1)
        {
            // disable the process button
            parameter.processEnabled = false;
        }

        var s = fileCount > 1 ? "s":""
        nbFiles.text = fileCount + " file" + s + " selected"

        var list = "";
        for (var i = 0; i < listAsArray.length; i++)
        {
            // for whatever reason, slice and delete
            // do not work for our purpose
            if (i !== fileIndex)
            {
                list += listAsArray[i];
                list += " ";
            }
        }

    }

    if (typeof(list) !== 'undefined') {
        fileList = list.trim();
    }
}

